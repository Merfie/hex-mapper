from tkinter import *
from tkinter import ttk

class Example(Frame):
    def __init__(self):
        self.world_sizes = [128, 256, 512, 1024, 2048]
        self.box_value = None
        self.v = None

    def main(self):
        # Create a new tkinter window
        root = Tk()
        root.geometry("1060x540")
        root.title("World Generator")
        # Initialize default string for worlds_dd and default int for world size
        self.box_value = StringVar()
        self.v = IntVar()
        self.v.set('512 px')
        self.box_value.set("New World")
        # make columns 0 and 1 dynamically adjust to size changes
        root.grid_columnconfigure(1, weight=1)
        root.grid_columnconfigure(0, weight=1)
        # Make the bottom row dynamically adjust to size changes, so next button is always in bottom right
        root.grid_rowconfigure(11, weight=1)


        img = PhotoImage(file="worldmap.png")
        panel = Label(root, image=img).grid(column=0, columnspan=1, row=0, rowspan=11, sticky='nsew', padx=10)
        name_entry = ttk.Entry(root)
        worlds = ('New World', 'Earth', 'Mars', 'Map_123456', 'Map_232323')
        ttk.Label(root, text="Select World").grid(column=1, row=0, sticky=W+S, padx=10)
        self.worlds_dd = ttk.Combobox(root, state='readonly', textvariable=self.box_value, values=worlds).grid(column=1, row=1, sticky='we', padx=10)
        ttk.Label(root, text="World Name").grid(column=1, row=2, sticky=W+S, padx=10)
        name_entry.grid(row=3,column=1, sticky='we', padx=10)
        Label(root, text="World Size").grid(column=1, row=4, sticky=W+S, padx=10)
        ttk.Radiobutton(root, text="128 px", variable=self.v, value=128).grid(column=1, row=5, sticky=W+N, padx=10)
        ttk.Radiobutton(root, text="256 px", variable=self.v, value=256).grid(column=1, row=6, sticky=W+N, padx=10)
        ttk.Radiobutton(root, text="512 px", variable=self.v, value=512).grid(column=1, row=7, sticky=W+N, padx=10)
        ttk.Radiobutton(root, text="1024 px", variable=self.v, value=1024).grid(column=1, row=8, sticky=W+N, padx=10)
        ttk.Radiobutton(root, text="2048 px", variable=self.v, value=2048).grid(column=1, row=9, sticky=W+N, padx=10)
        ttk.Button(root, text="Create Terrain").grid(column=1, row=10, sticky=W+E+N+S, padx=10)
        ttk.Button(root, text="Next").grid(column=1, row=11, sticky=S+E, padx=20, pady=40)
        root.mainloop()

    def callback(self, value):
        newvalue = min(self.world_sizes, key=lambda x: abs(x - float(value)))
        self.world_size.set(newvalue)



if __name__ == '__main__':
    example = Example()
    example.main()