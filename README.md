# README #

### What is this repository for? ###

** Quick summary**

Hex Mapper is a map generator built for generating world maps to use in a table top RPG.  The mapper makes heightmaps, factors in temperature, rainfall and wind to create a map of biomes.  

**Version**

v1.0 

I am willing to call the project acceptably done and want to share it with the public.  The wind simulation takes way too long for the lack luster results it provides and I may update it in the future.

### How do I get set up? ###

**Dependencies**

Hex Mapper uses the Python Imaging Library(PIL) to create the output images.  Hex Mapper also exports a JSON file that can be read by the Tiled Map Editor to see the biome information.

Download the repo into its own folder and run hexmapper.py.  This will create a folder called  seed_[SeedID] which will contain the image of the map at a scale of 1 hex = 1 pixel as well as a .json file for the Tiled Map Editor.  To open the file for tile you need to copy the biomes and terrains folder in root into the seed_[SeedID] folder.