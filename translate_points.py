from math import sqrt

x_rot = lambda x, z: (sqrt(3)/2 * x) + (z/2)
z_rot = lambda x, z: (sqrt(3)/2 * z) - (x/2)

BASE_A = .525731112119133606
BASE_B = .850650808352039932

initial_shape = [(-BASE_A, 0.0, BASE_B), (BASE_A, 0.0, BASE_B), (-BASE_A, 0.0, -BASE_B), (BASE_A, 0.0, -BASE_B),    
                   (0.0, BASE_B, BASE_A), (0.0, BASE_B, -BASE_A), (0.0, -BASE_B, BASE_A), (0.0, -BASE_B, -BASE_A),    
                   (BASE_B, BASE_A, 0.0), (-BASE_B, BASE_A, 0.0), (BASE_B, -BASE_A, 0.0), (-BASE_B, -BASE_A, 0.0)]

for vertex in initial_shape:
    print("({}, {}, {})".format(x_rot(vertex[0], vertex[2]), vertex[1], z_rot(vertex[0], vertex[2])))
