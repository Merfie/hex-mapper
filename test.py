import time
import random
import sys

from src.nodemapper.node_mapper import NodeMapper
from src.mapgenerators.heightmap import HeightMap
from src.mapgenerators.rivermap import RiverMap
from src.mapgenerators.biomemap import BiomeMap
from src.exporter.exporter import Exporter
from src.mapgenerators.temperaturemap import  TemperatureMap

sys.setrecursionlimit(50000000)

NUMBER_OF_TRIANGLES = 20
MAP_SEED = 986974846
#MAP_SEED = None
WATER_PERCENTAGE = .31
NUMBER_OF_RIVERS = 400
MOUNTAIN_HEIGHT = None

mapper = NodeMapper()
for i in range(0, 1):
    if MAP_SEED is None:
        MAP_SEED = random.randrange(0, 1000000000)
    start = time.time()
    mapper = NodeMapper(triangle_edge=513)
    height_mapper = HeightMap(mapper, size=513, seed_value=MAP_SEED)

    print("BULDING MAP: " + str(MAP_SEED))
    print("BUILDING HEIGHT MAP")
    height_mapper.make_world(NUMBER_OF_TRIANGLES)
    stop = time.time()
    print("Time to build heightmap: " + str(stop - start))
    start = time.time()
    print("SETTING SEA LEVEL")
    height_mapper.set_sea_level(WATER_PERCENTAGE)
    stop = time.time()
    print("Time to build sea level: " + str(stop - start))
    start = time.time()
    print("GETTING WORLD STATS")
    height_mapper.get_stats()
    stop = time.time()
    print("Time to build world stats: " + str(stop - start))
    print("GETTING WORLD TEMPS")
    start = time.time()
    temp_map = TemperatureMap(mapper, height_mapper.height_max, size=513)
    temp_map.set_temps()
    stop = time.time()
    print("Time to make temps: " + str(stop - start))
    # print("BUILDING RIVERS")
    # if MOUNTAIN_HEIGHT is None:
    #     MOUNTAIN_HEIGHT = int(height_mapper.height_max / 4)
    # river_map = RiverMap(mapper, number_of_rivers=NUMBER_OF_RIVERS, mountain_height=MOUNTAIN_HEIGHT)
    # river_map.set_sources()
    # river_map.build_rivers()
    # stop = time.time()
    # print("Time to build rivers: " + str(stop - start))
    # start = time.time()
    # print("BUILDING BIOMES")
    # biome_map = BiomeMap(mapper=mapper, min_height=height_mapper.height_min, max_height=height_mapper.height_max)
    # biome_map.generate_biomes()
    # stop = time.time()
    # print("Time to build biomes: " + str(stop - start))
    # start = time.time()
    # print("EXPORTING")
    exporter = Exporter(mapper, max_height=height_mapper.height_max, min_height=height_mapper.height_min, sea_level=height_mapper.sea_level, min_temp = temp_map.min_temp, max_temp = temp_map.max_temp)
    #exporter.export_large_world_map_for_tiled(map_seed=MAP_SEED)
    exporter.export_small_projected_heightmap(NUMBER_OF_TRIANGLES, edge_size=512, map_seed=MAP_SEED)
    exporter.export_small_projected_tempmap(NUMBER_OF_TRIANGLES, edge_size=512, map_seed=MAP_SEED)
    exporter.export_small_world_map_projection(map_seed=MAP_SEED)
    stop = time.time()
    print("Time to export: " + str(stop - start))
    start = time.time()

    print("Seed: " + str(MAP_SEED))



