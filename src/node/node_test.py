import unittest

from src.node.node import Node


class TestNode(unittest.TestCase):

    def setUp(self):
        self.test_node = Node()

    def test_create_node(self):
        self.test_node.add_coordinates(0, 0, 0)
        self.assertEqual(0, self.test_node.number_of_neighbors())
        self.assertIsNone(self.test_node.get_hex_neighbor_in_triangle(0, 0))
        self.assertIsNone(self.test_node.get_attribute("height"))

    def test_add_neighbor_with_index(self):
        test_node_tqr = (0, 1, 1)
        self.test_node.add_coordinates(*test_node_tqr)
        index0 = Node()
        index0.add_coordinates(0, 1, 0)
        self.test_node.add_neighbor_by_coordinate(index0, (0, 1, 0))
        index3 = Node()
        index3.add_coordinates(0, 1, 2)
        self.test_node.add_neighbor_by_coordinate(index3, (0, 1, 2))
        index4 = Node()
        index4.add_coordinates(0, 0, 2)
        self.test_node.add_neighbor_by_coordinate(index4, (0, 0, 2))

        self.assertEqual(index0, self.test_node.get_hex_neighbor_in_triangle(0, 0))
        self.assertEqual(index3, self.test_node.get_hex_neighbor_in_triangle(3, 0))
        self.assertEqual(index4, self.test_node.get_hex_neighbor_in_triangle(4, 0))
        self.assertEqual(index0, self.test_node.get_neighbor_by_coordinates(0, 1, 0))
        self.assertEqual(index3, self.test_node.get_neighbor_by_coordinates(0, 1, 2))
        self.assertEqual(index4, self.test_node.get_neighbor_by_coordinates(0, 0, 2))

        self.assertIsNone(self.test_node.get_hex_neighbor_in_triangle(2, 0))
        self.assertIsNone(self.test_node.get_hex_neighbor_in_triangle(1, 0))
        self.assertIsNone(self.test_node.get_hex_neighbor_in_triangle(5, 0))
        self.assertIsNone(self.test_node.get_neighbor_by_coordinates(0, 2, 0))
        self.assertIsNone(self.test_node.get_neighbor_by_coordinates(0, 2, 1))
        self.assertIsNone(self.test_node.get_neighbor_by_coordinates(0, 0, 1))
        self.assertEqual(3, self.test_node.number_of_neighbors())

    def test_add_corner_neighbor(self):
        self.test_node = Node(6)
        self.test_node.add_coordinates(0, 1, 0)
        neighbor = Node()
        neighbor.add_coordinates(0, 0, 0)
        self.test_node.add_corner_neighbor(neighbor, (0, 0, 0))
        self.assertEqual(1, self.test_node.number_of_neighbors())

        self.test_node.add_corner_neighbor(neighbor, (0, 0, 0))
        self.assertEqual(1, self.test_node.number_of_neighbors())

    def test_add_q_r_neighbors(self):
        self.test_node = Node(5)
        tqr = (0, 0, 0)
        self.test_node.add_coordinates(*tqr)
        q_neigh = Node()
        q_neigh.add_coordinates(0, 1, 0)
        r_neigh = Node()
        r_neigh.add_coordinates(0, 0, 1)

        self.test_node.add_q_neighbor(q_neigh, tqr)
        self.test_node.add_r_neighbor(r_neigh, tqr)

        self.assertEqual(self.test_node.get_q_neighbor(0), q_neigh)
        self.assertEqual(self.test_node.get_r_neighbor(0), r_neigh)

    def test_add_too_many_neighbors(self):
        self.test_node.add_coordinates(0, 0, 0)
        for x in range(5):
            node = Node()
            node.add_coordinates(0, 0, x)
            self.test_node.add_neighbor_by_coordinate(node, (0, 0, x))

        self.assertIsNone(self.test_node.get_hex_neighbor_in_triangle(6, 0))

    def test_too_many_neighbors_on_pentagon(self):
        five_node = Node(5)
        five_node.add_coordinates(0, 0, 0)
        five_node.add_q_neighbor(Node(), (0, 0, 0))
        five_node.add_r_neighbor(Node(), (0, 0, 0))
        for x in range(2):
            node = Node()
            node.add_coordinates(0, 1, x)
            five_node.add_neighbor_by_coordinate(node, (0, 1, x))

        self.assertIsNotNone(five_node.get_corner_neighbor_in_triangle("q", 0))
        self.assertIsNone(five_node.get_corner_neighbor_in_triangle("q", 1))

    def test_attribute(self):
        self.test_node.set_attribute("height", 56)
        self.assertEqual(56, self.test_node.get_attribute("height"))

    def test_setting_coordinates(self):
        self.test_node.add_coordinates(0, 0, 0)
        self.test_node.add_coordinates(1, 0, 0)
        self.assertTrue(self.test_node.is_coordinates(1, 0, 0))
        self.assertTrue(self.test_node.is_coordinates(0, 0, 0))
        self.assertFalse(self.test_node.is_coordinates(0, 4, 4))

    def test_node_is_triangle(self):
        self.test_node.add_coordinates(1, 0, 0)
        self.test_node.add_coordinates(1, 1, 1)
        self.test_node.add_coordinates(0, 1, 2)

        self.assertTrue(self.test_node.is_triangle(0))
        self.assertTrue(self.test_node.is_triangle(1))
        self.assertFalse(self.test_node.is_triangle(2))

    def test_get_triangle_coordinates(self):
        self.test_node.add_coordinates(1, 0, 0)
        self.test_node.add_coordinates(0, 1, 0)

        print(self.test_node.get_coordinates_for_triangle(1))
        self.assertEqual(self.test_node.get_coordinates_for_triangle(1), (1, 0, 0))

    def test_node_equality(self):
        self.test_node.add_coordinates(0, 0, 0)
        self.test_node.set_attribute("height", 50)

        self.assertTrue(self.test_node in [self.test_node])
        self.assertTrue(self.test_node is self.test_node)

        faker = Node()
        faker.add_coordinates(0, 0, 0)
        self.assertFalse(faker in [self.test_node])
        self.assertFalse(faker is self.test_node)
        self.assertFalse(faker == self.test_node)

    def test_coordinate_in_node(self):
        self.test_node.add_coordinates(0, 0, 0)
        self.test_node.add_coordinates(1, 0, 0)
        self.assertTrue((0, 0, 0) in self.test_node)
        self.assertTrue((1, 0, 0) in self.test_node)
        self.assertFalse((0, 1, 1) in self.test_node)

    def test_coordinate_in_node_list(self):
        self.test_node.add_coordinates(0, 0, 0)
        self.test_node.add_coordinates(1, 0, 0)
        self.assertTrue((0, 0, 0) in [self.test_node])

if __name__ == '__main__':
    unittest.main()
