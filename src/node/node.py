def neighbor_lot(source_qr, direction_qr):
    return tuple(x + y for x, y in zip(source_qr, direction_qr))


top_neighbor_qr = {"q": (0, 1, 0), "r": (0, 0, 1)}
left_neighbor_qr = {"q": (0, 1, -1), "r": (0, 0, -1)}
right_neighbor_qr = {"q": (0, -1, 0), "r": (0, -1, 1)}

hex_neighbor_qr = {0: (0, 0, -1),
                   1: (0, 1, -1),
                   2: (0, 1, 0),
                   3: (0, 0, 1),
                   4: (0, -1, 1),
                   5: (0, -1, 0)}


class Node(object):
    """
     A node class for representing hexagons and pentagons
     By default, it will produce a hexagon
     The sides are numbered starting from the top and going in a clockwise fashion
     0-indexed
     All neighbors are initialized to None

     TODO: investigate whether you actually need that "discovered edges" part
    """

    def __init__(self, max_neighbors=6):
        self.max_neighbors = max_neighbors
        self.neighbors = {}
        self.attributes = {"height": None, "river": False, "temperature": None, "precipitation": 1, "biome": 0, "terrain": 0, "wind": 0}
        self.coordinates = []

    def __contains__(self, item):
        if isinstance(item, tuple):
            return item in self.coordinates

    def __eq__(self, other):
        if isinstance(other, tuple):
            return other in self.coordinates
        elif isinstance(other, Node):
            return other.coordinates == self.coordinates and other.attributes == self.attributes

    def __hash__(self):
        # TODO work on this holy crap its a bad hash
        for key in self.attributes.keys():
            hashValue = hash(self.attributes.get(key))

        for tqr in self.coordinates:
            if tqr is not None:
                hashValue = hashValue ^ hash(tqr)

        return hashValue

    def number_of_neighbors(self):
        """
        :return: the number of neighbors occupied by a Node
        """
        return len(set(self.neighbors.values()))

    def get_hex_neighbor_in_triangle(self, neighbor_index, triangle):
        """
        :param neighbor_index: index of the neighbor desired.
        :param triangle: the triangle that this node indentifies as for neighbor lookup
                See class docstring for numbering scheme
        :return: the Node that is a neighbor, or None if it doesn't exist
        """

        try:
            assert neighbor_index < self.max_neighbors
            neighbor_tqr = neighbor_lot(self.get_coordinates_for_triangle(triangle),
                                        hex_neighbor_qr.get(neighbor_index))
            return self.neighbors[neighbor_tqr]
        except (KeyError, AssertionError) as e:
            return None

    def add_neighbor_by_coordinate(self, neighbor, neighbor_tqr):
        """
        adds a neighbor at the specified index.
        Will overwrite whatever is there
        :param neighbor_index: Index to add the neighbor Node
        :param neighbor: the neighbor Node to add
        :param node_tqr: The coordinates of the node you're adding the neighbor to
        :return:
        """
        t = neighbor_tqr[0]
        q = neighbor_tqr[1]
        r = neighbor_tqr[2]
        direction = None
        toggle = 1
        if t in (5, 7, 9, 11, 13, 15, 16, 17, 18, 19):
            toggle = -1
        #Literal edge case garbage will go here
        if self.max_neighbors is 6 and ((self.coordinates[0][1] is 0 and self.coordinates[0][2] is not 0) or (self.coordinates[0][1] is not 0 and self.coordinates[0][2] is 0)) and self.coordinates[0][0] in (0, 1, 2, 3, 4, 15, 16, 17, 18, 19):
            if ((t, q, r + toggle) in self.coordinates or (t, q + toggle, r) in self.coordinates) and (q is 0 or r is 0):
                direction = "north"
            elif ((t, q, r - toggle) in self.coordinates or (t, q - toggle, r) in self.coordinates) and (q is 0 or r is 0):
                direction = "south"
            elif (t, q - toggle, r + toggle) in self.coordinates:
                direction = "east"
            elif (t, q + toggle, r - toggle) in self.coordinates:
                direction = "west"
            elif (t, q - toggle, r) in self.coordinates:
                direction = "southeast"
            elif (t, q, r - toggle) in self.coordinates:
                direction = "southwest"
            elif (t, q + toggle, r) in self.coordinates:
                direction = "northeast"
            elif (t, q, r + toggle) in self.coordinates:
                direction = "northwest"
        #Literal corner cases go here
        elif self.max_neighbors is 5:
            if (0, 0, 0) in self.coordinates:
                direction = "south"
            elif (15, 0, 0) in self.coordinates:
                direction = "north"
            elif self.coordinates[0][0] in (0, 1, 2, 3, 4):
                if (t, q - toggle, r + toggle) in self.coordinates:
                    direction = "east"
                elif (t, q + toggle, r - toggle) in self.coordinates:
                    direction = "west"
                elif (t, q - toggle, r) in self.coordinates:
                    direction = "southeast"
                elif (t, q, r - toggle) in self.coordinates:
                    direction = "southwest"
                else:
                    direction = "north"
            elif self.coordinates[0][0] in (5, 6, 8, 10, 12):
                if (t, q - toggle, r + toggle) in self.coordinates:
                    direction = "east"
                elif (t, q + toggle, r - toggle) in self.coordinates:
                    direction = "west"
                elif (t, q + toggle, r) in self.coordinates:
                    direction = "northeast"
                elif (t, q, r + toggle) in self.coordinates:
                    direction = "northwest"
                else:
                    direction = "south"
        #Nice and normal cases go here
        else:
            if (t, q - toggle, r + toggle) in self.coordinates:
                direction = "east"
            elif (t, q + toggle, r - toggle) in self.coordinates:
                direction = "west"
            elif (t, q + toggle, r) in self.coordinates:
                direction = "northwest"
            elif (t, q, r + toggle) in self.coordinates:
                direction = "northeast"
            elif (t, q - toggle, r) in self.coordinates:
                direction = "southeast"
            elif (t, q, r - toggle) in self.coordinates:
                direction = "southwest"
        if direction is not None:
            if direction in self.neighbors:
                i = 1
                while direction + "_" + str(i) in self.neighbors:
                    i += 1
                direction = direction + "_" + str(i)
            self.neighbors[direction] = neighbor
        self.neighbors[neighbor_tqr] = neighbor

    def add_corner_neighbor(self, neighbor, neighbor_tqr):
        if self.number_of_neighbors() is self.max_neighbors:
             raise ValueError("All the edges are occupied " + str(self.coordinates) + " " + str(neighbor_tqr))
        if neighbor in self.neighbors.values():
            return

        #self.neighbors[neighbor_tqr] = neighbor
        #consolidating my work here - Merfie
        self.add_neighbor_by_coordinate(neighbor, neighbor_tqr)

    def get_attribute(self, attribute):
        return self.attributes.get(attribute)

    def set_attribute(self, attribute_name, attribute_value):
        self.attributes[attribute_name] = attribute_value

    def add_coordinates(self, triangle, q, r):
        if (triangle, q, r) not in self.coordinates:
            self.coordinates.append((triangle, q, r))

    def is_coordinates(self, triangle, q, r):
        """
        Method to check if the Node can be represented with the given coordinates
        :param triangle: the triangle the Node could be in
        :param q: the q coordinate
        :param r: the x coordinate
        :return: True if the tuple (t, q, r) is in the list of coordinates otherwise False
        """
        return (triangle, q, r) in self.coordinates

    def is_triangle(self, triangle):
        """
        Method to check if the Node exists in a triangle
        :param triangle: triangle key that to query on
        :return: True if the Node is in the given triangle, else False
        """
        return True if self.get_coordinates_for_triangle(triangle) is not None else False

    def get_coordinates_for_triangle(self, triangle):
        """
        Method to get the coordinate representation for a triangle
        :param triangle: the desired triangle you want the coordinates for
        :return: a tuple with the requested coordinates
        """
        try:
            return next((x for x in self.coordinates if (triangle,) == x[:1]))
        except StopIteration:
            return None

    def get_neighbor_by_coordinates(self, t, q, r):
        return self.neighbors.get((t, q, r))

    def add_q_neighbor(self, neighbor, node_tqr):
        return self.add_corner_neighbor(neighbor, neighbor_lot(node_tqr, self.corner_neighbor_coordinates_for_tqr(node_tqr).get("q")))

    def add_r_neighbor(self, neighbor, node_tqr):
        return self.add_corner_neighbor(neighbor, neighbor_lot(node_tqr, self.corner_neighbor_coordinates_for_tqr(node_tqr).get("r")))

    def corner_neighbor_coordinates_for_tqr(self, node_tqr):
        if self.is_top_corner(node_tqr):
            return top_neighbor_qr
        elif self.is_left_corner(node_tqr):
            return left_neighbor_qr
        elif self.is_right_corner(node_tqr):
            return right_neighbor_qr
        else:
            return None

    def get_q_neighbor(self, triangle):
        tqr = self.get_coordinates_for_triangle(triangle)

        if tqr is None:
            return None

        return self.get_neighbor_by_coordinates(*neighbor_lot(tqr, self.corner_neighbor_coordinates_for_tqr(tqr).get("q")))

    def get_r_neighbor(self, triangle):
        tqr = self.get_coordinates_for_triangle(triangle)

        if tqr is None:
            return None

        return self.get_neighbor_by_coordinates(*neighbor_lot(tqr, self.corner_neighbor_coordinates_for_tqr(tqr).get("r")))

    def is_top_corner(self, tqr):
        self.assert_corner()
        return tqr == (tqr[0], 0, 0)

    def is_left_corner(self, tqr):
        self.assert_corner()
        return tqr == (tqr[0], 0, tqr[2])

    def is_right_corner(self, tqr):
        self.assert_corner()
        return tqr == (tqr[0], tqr[1], 0)

    def assert_corner(self):
        assert self.max_neighbors is 5

    def assert_hex(self):
        assert self.max_neighbors is 6

    def get_corner_neighbor_in_triangle(self, qr, triangle):
        assert qr is "q" or "r"
        return self.get_q_neighbor(triangle) if qr == "q" else self.get_r_neighbor(triangle)
