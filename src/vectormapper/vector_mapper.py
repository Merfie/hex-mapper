
from math import sqrt


class VectorMapper(object):

    def __init__(self, node_map):
        self.node_map = node_map

    def initialize_corners(self):
        for node, vectors in self.initial_vectors(self.node_map.triangle_edge_length - 1).items():
            target = self.node_map.get_node(*node)
            target.set_attribute("vector_x", vectors[0])
            target.set_attribute("vector_y", vectors[1])
            target.set_attribute("vector_z", vectors[2])

    def initial_vectors(self, size):
        return {
            (0, 0, 0): (-0.02997109447899471, 0.0, 0.9995507658422011),
            (0, 0, size): (0.8806219028310346, 0.0, 0.4738196537230676),
            (0, size, 0): (0.2628655560595668, 0.8506508083520399, 0.45529649865501465),
            (1, size, 0): (-0.7366852097826344, 0.5257311121191336, 0.42532540417601994),
            (2, size, 0): (-0.7366852097826344, -0.5257311121191336, 0.42532540417601994),
            (3, size, 0): (0.2628655560595668, -0.8506508083520399, 0.45529649865501465),
            (5, 0, 0): (0.7366852097826344, 0.5257311121191336, -0.42532540417601994),
            (6, size, 0): (-0.2628655560595668, 0.8506508083520399, -0.45529649865501465),
            (8, size, 0): (-0.8806219028310346, 0.0, -0.4738196537230676),
            (10, size, 0): (-0.2628655560595668, -0.8506508083520399, -0.45529649865501465),
            (12, size, 0): (0.7366852097826344, -0.5257311121191336, -0.42532540417601994),
            (15, 0, 0): (0.02997109447899471, 0.0, -0.9995507658422011)
        }

    def calculate_vector_at_distance(self, node_1, node_2, distance):
        x = (1 - distance) * float(node_1.get_attribute("vector_x")) + \
            distance * float(node_2.get_attribute("vector_x"))
        y = (1 - distance) * float(node_1.get_attribute("vector_y")) + \
            distance * float(node_2.get_attribute("vector_y"))
        z = (1 - distance) * float(node_1.get_attribute("vector_z")) + \
            distance * float(node_2.get_attribute("vector_z"))

        return (x, y, z)

    def normalize_vector(self, x, y, z):
        magnitude = sqrt(
            (x ** 2) + (y ** 2) + (z ** 2)
        )
        return [i * magnitude for i in (x, y, z)]

    def vectorize_all_nodes(self):
        self.initialize_corners()

        max_index = self.node_map.triangle_edge_length - 1
        for i in range(0, 20):
            # Initialize edges
            # I could do this separately, but I want to iterate as few times as possible
            corner1 = self.node_map.get_node(i, 0, 0)
            corner2 = self.node_map.get_node(i, 0, max_index)
            corner3 = self.node_map.get_node(i, max_index, 0)

            # Checking if corner1 to corner2 is already calculated
            if self.node_map.get_node(i, 0, 1).get_attribute("vector_x") is None:
                # This edge is not yet filled in, starting at 1 to skip the corner
                for r in range(1, max_index):
                    distance = r / max_index
                    vector = self.calculate_vector_at_distance(corner1, corner2, distance)
                    vector = self.normalize_vector(*vector)
                    self.write_vector(*vector, node=self.node_map.get_node(i, 0, r))

            # Checking if corner1 to corner3 is already calculated
            if self.node_map.get_node(i, 1, 0).get_attribute("vector_x") is None:
                for q in range(1, max_index):
                    distance = q / max_index
                    vector = self.calculate_vector_at_distance(corner1, corner3, distance)
                    vector = self.normalize_vector(*vector)
                    self.write_vector(*vector, node=self.node_map.get_node(i, q, 0))

            # Checking if corner 2 to corner 3 is already calculated
            if self.node_map.get_node(i, 1, max_index - 1).get_attribute("vector_x") is None:
                for qr in range(1, max_index):
                    q = qr
                    r = max_index - qr
                    distance = q / max_index
                    vector = self.calculate_vector_at_distance(corner2, corner3, distance)
                    vector = self.normalize_vector(*vector)
                    self.write_vector(*vector, node=self.node_map.get_node(i, q, r))

            # Starting at 2 because the corner and the first row have no distances to calculate
            for qr in range(2, max_index):
                source = self.node_map.get_node(i, 0, qr)
                dest = self.node_map.get_node(i, qr, 0)

                # Pick the edges and fill in the row between them
                for distance in range(1, qr):
                    target = self.node_map.get_node(i, distance, qr - distance)
                    vector = self.calculate_vector_at_distance(source, dest, distance / qr)
                    self.write_vector(*self.normalize_vector(*vector), node=target)

    def write_vector(self, x, y, z, node=None):
        node.set_attribute("vector_x", x)
        node.set_attribute("vector_y", y)
        node.set_attribute("vector_z", z)
