import unittest

from src.vectormapper.vector_mapper import VectorMapper
from src.nodemapper.node_mapper import NodeMapper
from src.mapgenerators.heightmap import HeightMap

MAP_SEED = 986974846


class TestVectorMapper(unittest.TestCase):

    def setUp(self):
        self.node_map = NodeMapper(5)
        self.height_map = HeightMap(self.node_map, size=5, seed_value=MAP_SEED)
        self.height_map.make_world(20)
        self.test_vector_map = VectorMapper(self.node_map)

    def test_should_assign_corner_nodes_xyz(self):
        self.test_vector_map.initialize_corners()
        self.assertEqual(self.node_map.get_node(0, 0, 0).get_attribute("vector_x"), -0.02997109447899471)
        self.assertEqual(self.node_map.get_node(1, 0, 0).get_attribute("vector_x"), -0.02997109447899471)
        self.assertEqual(self.node_map.get_node(2, 0, 0).get_attribute("vector_x"), -0.02997109447899471)
        self.assertEqual(self.node_map.get_node(3, 0, 0).get_attribute("vector_x"), -0.02997109447899471)
        self.assertEqual(self.node_map.get_node(4, 0, 0).get_attribute("vector_x"), -0.02997109447899471)

    def test_should_populate_triangle_xyz(self):
        self.test_vector_map.vectorize_all_nodes()
        # test edges
        self.assertIsNotNone(self.node_map.get_node(0, 1, 0).get_attribute("vector_x"))
        self.assertIsNotNone(self.node_map.get_node(0, 2, 0).get_attribute("vector_x"))
        self.assertIsNotNone(self.node_map.get_node(0, 3, 0).get_attribute("vector_x"))

        self.assertIsNotNone(self.node_map.get_node(0, 0, 1).get_attribute("vector_x"))
        self.assertIsNotNone(self.node_map.get_node(0, 0, 2).get_attribute("vector_x"))
        self.assertIsNotNone(self.node_map.get_node(0, 0, 3).get_attribute("vector_x"))

        self.assertIsNotNone(self.node_map.get_node(0, 1, 3).get_attribute("vector_x"))
        self.assertIsNotNone(self.node_map.get_node(0, 2, 2).get_attribute("vector_x"))
        self.assertIsNotNone(self.node_map.get_node(0, 3, 1).get_attribute("vector_x"))

        # test center
        self.assertIsNotNone(self.node_map.get_node(0, 1, 1).get_attribute("vector_x"))
        self.assertIsNotNone(self.node_map.get_node(0, 1, 2).get_attribute("vector_x"))
        self.assertIsNotNone(self.node_map.get_node(0, 2, 1).get_attribute("vector_x"))

        self.assertIsNotNone(self.node_map.get_node(0, 1, 3).get_attribute("vector_x"))
        self.assertIsNotNone(self.node_map.get_node(0, 2, 2).get_attribute("vector_x"))
        self.assertIsNotNone(self.node_map.get_node(0, 3, 1).get_attribute("vector_x"))

    def test_should_calculate_distance_between_two_nodes(self):
        node1 = self.node_map.get_node(0, 0, 0)
        node1.set_attribute("vector_x", 1)
        node1.set_attribute("vector_y", 1)
        node1.set_attribute("vector_z", 1)

        node2 = self.node_map.get_node(0, 0, 4)
        node2.set_attribute("vector_x", 5)
        node2.set_attribute("vector_y", 5)
        node2.set_attribute("vector_z", 5)

        midpoint = self.test_vector_map.calculate_vector_at_distance(node1, node2, .5)
        self.assertEqual(midpoint[0], 3)
        self.assertEqual(midpoint[1], 3)
        self.assertEqual(midpoint[2], 3)

        quarterpoint = self.test_vector_map.calculate_vector_at_distance(node1, node2, (1 / 4))
        self.assertEqual(quarterpoint[0], 2)
        self.assertEqual(quarterpoint[1], 2)
        self.assertEqual(quarterpoint[2], 2)

        farpoint = self.test_vector_map.calculate_vector_at_distance(node1, node2, (3 / 4))
        self.assertEqual(farpoint[0], 4)
        self.assertEqual(farpoint[1], 4)
        self.assertEqual(farpoint[2], 4)

        # Sanity check test
        node2point = self.test_vector_map.calculate_vector_at_distance(node1, node2, 1)
        self.assertEqual(node2point[0], 5)
        self.assertEqual(node2point[1], 5)
        self.assertEqual(node2point[2], 5)


if __name__ == '__main__':
    unittest.main()
