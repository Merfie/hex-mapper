import random
import heapq


class BiomeMap(object):

    def __init__(self, mapper, max_height, min_height):
        self.mapper = mapper
        self.latitude_per_hex = (180/(self.mapper.triangle_edge_length * 3))
        self.max_height = max_height
        self.min_height = min_height
        self.cold_height = int((self.max_height/5) * 2)
        self.deep_depth = int((self.min_height/3) * 2)
        self.mid_depth = int((self.min_height/3))
        self.mountain_height = 0
        self.hill_height = 0
        self.max_temp = 0
        self.min_temp = 0
        self.max_rain = 0
        self.min_rain = 1000000
        self.edge_length = 0
        self.temp_box_1 = 0
        self.temp_box_2 = 0
        self.temp_box_3 = 0
        self.temp_box_4 = 0
        self.temp_box_5 = 0
        self.temp_box_6 = 0
        self.temp_box_7 = 0
        self.temp_box_8 = 0
        self.temp_box_9 = 0
        self.snow_count = 0
        self.tundra_count = 0
        self.taiga_count = 0
        self.grasslands_count = 0
        self.cooldesert_count = 0
        self.desert_count = 0
        self.hotdesert_count = 0
        self.forest_count = 0
        self.jungle_count = 0
        self.precip_count_1 = 0
        self.precip_count_2 = 0
        self.precip_count_3 = 0
        self.precip_count_4 = 0
        self.precip_count_5 = 0
        self.precip_count_6 = 0
        self.precip_count_7 = 0
        self.precip_count_8 = 0
        self.precip_count_9 = 0
        self.precip_count_10 = 0

    def generate_biomes(self):
        counter = 0
        self.generate_starting_values()
        print("SIMULATING WIND, THIS MAY TAKE FOREVER")
        for node in sorted(self.mapper.nodes.values(), key=lambda node: node.get_attribute("height")):
            self.simulate_wind(node, 20)
            counter += 1
        print("Smoothing Biomes")
        for node in sorted(self.mapper.nodes.values(), key=lambda node: node.get_attribute("temperature")):
            node.set_attribute("temperature", self.average_temperature(node.neighbors.values()))
        for node in sorted(self.mapper.nodes.values(), key=lambda node: node.get_attribute("precipitation")):
            node.set_attribute("precipitation", self.average_precipitation(node.neighbors.values()))
        print("Getting Min and Max")
        self.get_min_max()
        print("Max Rain: " + str(self.max_rain))
        print("Min Rain: " + str(self.min_rain))
        print("Max Temp: " + str(self.max_temp))
        print("Min Temp: " + str(self.min_temp))
        print("Calculating boxes")
        attr=[o.get_attribute("temperature") for o in self.mapper.nodes.values()]
        self.temp_box_1 = max(heapq.nsmallest(int(len(self.mapper.nodes)/10), attr))
        self.temp_box_2 = max(heapq.nsmallest(int(len(self.mapper.nodes)/10) * 2, attr))
        self.temp_box_3 = max(heapq.nsmallest(int(len(self.mapper.nodes)/10) * 3, attr))
        self.temp_box_4 = max(heapq.nsmallest(int(len(self.mapper.nodes)/10) * 4, attr))
        self.temp_box_5 = max(heapq.nsmallest(int(len(self.mapper.nodes)/10) * 5, attr))
        self.temp_box_6 = max(heapq.nsmallest(int(len(self.mapper.nodes)/10) * 6, attr))
        self.temp_box_7 = max(heapq.nsmallest(int(len(self.mapper.nodes)/10) * 7, attr))
        self.temp_box_8 = max(heapq.nsmallest(int(len(self.mapper.nodes)/10) * 8, attr))
        self.temp_box_9 = max(heapq.nsmallest(int(len(self.mapper.nodes)/10) * 9, attr))
        land_nodes=[o.get_attribute("height") for o in self.mapper.nodes.values() if o.get_attribute("height") > 0]
        self.mountain_height = max(heapq.nsmallest(int(len(land_nodes)/20) * 19, land_nodes))
        self.hill_height = max(heapq.nsmallest(int(len(land_nodes)/20) * 15, land_nodes))
        print("MOUNTAIN HEIGHT: " + str(self.mountain_height))
        print("HILL HEIGHT: " + str(self.hill_height))
        print("Setting Biomes")
        precip_box = 1
        for box in self.chunks(sorted(self.mapper.nodes.values(), key=lambda node: node.get_attribute("precipitation")), int(len(self.mapper.nodes)/10)):
            for node in box:
                temp_box = 0
                if node.get_attribute("temperature") <= self.temp_box_1:
                    temp_box = 1
                elif node.get_attribute("temperature") <= self.temp_box_2:
                    temp_box = 2
                elif node.get_attribute("temperature") <= self.temp_box_3:
                    temp_box = 3
                elif node.get_attribute("temperature") <= self.temp_box_4:
                    temp_box = 4
                elif node.get_attribute("temperature") <= self.temp_box_5:
                    temp_box = 5
                elif node.get_attribute("temperature") <= self.temp_box_6:
                    temp_box = 6
                elif node.get_attribute("temperature") <= self.temp_box_7:
                    temp_box = 7
                elif node.get_attribute("temperature") <= self.temp_box_8:
                    temp_box = 8
                elif node.get_attribute("temperature") <= self.temp_box_9:
                    temp_box = 9
                else:
                    temp_box = 10
                self.set_biome(node, temp_box, precip_box)
            precip_box += 1

        # for node in sorted(self.mapper.nodes.values(), key=lambda node: node.get_attribute("precipitation")):
        #     self.set_biome(node, counter, len(self.mapper.nodes))
        total = self.snow_count + self.tundra_count + self.taiga_count + self.cooldesert_count + self.desert_count + self.hotdesert_count + self.grasslands_count + self.forest_count + self.jungle_count
        print("SNOW COUNT: " + str(self.snow_count * 100/total) + "%")
        print("TUNDRA COUNT: " + str(self.tundra_count * 100/total) + "%")
        print("TAIGA COUNT: " + str(self.taiga_count * 100/total) + "%")
        print("COOL DESERT COUNT: " + str(self.cooldesert_count * 100/total) + "%")
        print("DESERT COUNT: " + str(self.desert_count * 100 /total) + "%")
        print("HOT DESERT COUNT: " + str(self.hotdesert_count * 100 /total) + "%")
        print("GRASSLAND COUNT: " + str(self.grasslands_count * 100 /total) + "%")
        print("FOREST COUNT: " + str(self.forest_count * 100 /total) + "%")
        print("JUNGLE COUNT: " + str(self.jungle_count * 100 /total) + "%")

    def chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i+n]

    def get_min_max(self):
        self.max_temp = max([o.get_attribute("temperature") for o in self.mapper.nodes.values()])
        self.min_temp = min([o.get_attribute("temperature") for o in self.mapper.nodes.values()])
        self.max_rain = max([o.get_attribute("precipitation") for o in self.mapper.nodes.values()])
        self.min_rain = min([o.get_attribute("precipitation") for o in self.mapper.nodes.values()])

    def set_biome(self, node, temp_box, precip_box):
        # snow
        if((temp_box is 1) and (precip_box in (1, 2, 3, 4))) or ((temp_box is 2) and (precip_box in (1, 2, 3))):
            node.set_attribute("biome", 10)
            if node.get_attribute("height") >= self.mountain_height:
                node.set_attribute("terrain", 27)
            elif node.get_attribute("height") >= self.hill_height:
                node.set_attribute("terrain", 23)
            self.snow_count += 1
        #tundra
        elif((temp_box is 1) and (precip_box in (5, 6, 7, 8, 9, 10))):
            node.set_attribute("biome", 12)
            if node.get_attribute("height") >= self.mountain_height:
                node.set_attribute("terrain", 27)
            elif node.get_attribute("height") >= self.hill_height:
                node.set_attribute("terrain", 23)
            self.tundra_count += 1
        # Deep Water
        elif node.get_attribute("height") < self.deep_depth:
            node.set_attribute("biome", 2)
        # medium water
        elif node.get_attribute("height") < self.mid_depth:
            node.set_attribute("biome", 8)
        # shallow water
        elif node.get_attribute("height") <= 0 or node.get_attribute("river"):
            node.set_attribute("biome", 9)

        #taiga
        elif((temp_box is 2) and (precip_box in (4, 5, 6, 7, 8, 9, 10))) or ((temp_box is 3) and (precip_box in (4, 5, 6, 7, 8, 9, 10))) or ((temp_box is 4) and (precip_box in (9, 10))):
            node.set_attribute("biome", 11)
            if node.get_attribute("height") >= self.mountain_height:
                node.set_attribute("terrain", 17)
            elif node.get_attribute("height") >= self.hill_height:
                node.set_attribute("terrain", 16)
            else:
                node.set_attribute("terrain", 15)
            self.taiga_count += 1
        #cool desert
        elif((temp_box is 4) and (precip_box in (1, 2))) or ((temp_box is 3) and (precip_box in (1, 2, 3))):
            node.set_attribute("biome", 1)
            if node.get_attribute("height") >= self.hill_height:
                node.set_attribute("terrain", 23)
            self.cooldesert_count += 1
        #grasslands
        elif((temp_box is 4) and (precip_box in (3, 4, 5, 6, 7, 8))) or ((temp_box is 5) and (precip_box in (2, 3, 4, 5, 6, 7, 8, 9))) or ((temp_box is 6) and (precip_box in (3, 4, 5, 6, 7, 8))) or ((temp_box is 7) and (precip_box in (4, 5, 6, 7))) or ((temp_box is 8) and precip_box in (5, 6)) or ((temp_box is 9) and precip_box is 5):
            node.set_attribute("biome", 5)
            if node.get_attribute("height") >= self.mountain_height:
                node.set_attribute("terrain", 27)
            elif node.get_attribute("height") >= self.hill_height:
                node.set_attribute("terrain", 21)
            else:
                node.set_attribute("terrain", 20)
            self.grasslands_count += 1
        #hot desert
        elif((temp_box is 7) and (precip_box is 1)) or ((temp_box is 8) and (precip_box is 1)) or ((temp_box is 9) and (precip_box in (1, 2))) or ((temp_box is 10) and (precip_box in (1, 2))) or ((temp_box is 11) and (precip_box in (1, 2))):
            node.set_attribute("biome", 6)
            if node.get_attribute("height") >= self.mountain_height:
                node.set_attribute("terrain", 27)
            elif node.get_attribute("height") >= self.hill_height:
                node.set_attribute("terrain", 14)
            self.hotdesert_count += 1
        #desert
        elif((precip_box is 1) and (temp_box in (5, 6))) or ((precip_box is 2) and (temp_box in (6, 7, 8))) or ((precip_box is 3) and (temp_box in (7, 8, 9, 10, 11))) or ((precip_box is 4) and (temp_box in (8, 9, 10, 11))):
            node.set_attribute("biome", 3)
            if node.get_attribute("height") >= self.mountain_height:
                node.set_attribute("terrain", 27)
            elif node.get_attribute("height") >= self.hill_height:
                node.set_attribute("terrain", 14)
            self.desert_count += 1
        #forests
        elif((temp_box is 5) and (precip_box is 10)) or ((temp_box is 6) and (precip_box in (9, 10))) or ((temp_box is 7) and (precip_box in (8, 9, 10))) or ((temp_box is 8) and (precip_box in (7, 8, 9, 10))) or ((temp_box is 9) and (precip_box in (6, 7, 8))) or ((temp_box is 10) and (precip_box in(5, 6, 7))) or ((temp_box is 11) and (precip_box in(5, 6, 7))):
            node.set_attribute("biome", 4)
            if node.get_attribute("height") >= self.mountain_height:
                node.set_attribute("terrain", 19)
            elif node.get_attribute("height") >= self.hill_height:
                node.set_attribute("terrain", 18)
            else:
                node.set_attribute("terrain", 26)
            self.forest_count += 1
        #jungle
        elif((temp_box is 9) and (precip_box in (9, 10))) or ((temp_box is 10) and (precip_box in (8, 9, 10))) or ((temp_box is 11) and (precip_box in (8, 9, 10))):
            node.set_attribute("biome", 7)
            if node.get_attribute("height") >= self.hill_height:
                node.set_attribute("terrain", 25)
            else:
                node.set_attribute("terrain", 24)
            self.jungle_count += 1

    def generate_starting_values(self):
        lat = 0
        node = None
        self.edge_length = self.mapper.triangle_edge_length - 1
        for chunk in range(0, 3):
            if chunk is 0:
                for y in range(0, self.edge_length + 1):
                    for triangle in range(0, 5):
                        for x in range(0, y + 1):
                            node = self.mapper.get_node_by_xy(triangle, x, y)
                            lat = y
                            self.set_temperature(node, lat)
                            self.set_wind(node, y)
                            self.set_precipitation(node, y)
            elif chunk is 1:
                for y in range(0, self.edge_length + 1):
                    for triangle in range(5, 15):
                        if triangle % 2 is 0:
                            for x in range(0, y + 1):
                                node = self.mapper.get_node_by_xy(triangle, x, y)
                                lat = self.edge_length + y
                                if lat > self.edge_length * 1.5:
                                    lat = (self.edge_length * 1.5) - (lat % (self.edge_length * 1.5))
                                self.set_temperature(node, lat)
                                self.set_wind(node, self.edge_length + y)
                                self.set_precipitation(node, self.edge_length + y)
                        else:
                            y = self.edge_length - y
                            for x in range(0, y):
                                node = self.mapper.get_node_by_xy(triangle, x, y)
                                lat = self.edge_length + y
                                if lat > self.edge_length * 1.5:
                                    lat = (self.edge_length * 1.5) - (lat % (self.edge_length * 1.5))
                                self.set_temperature(node, lat)
                                self.set_wind(node, self.edge_length + (self.edge_length - y))
                                self.set_precipitation(node, self.edge_length + (self.edge_length - y))
            elif chunk is 2:
                for y in range(self.edge_length, -1, -1):
                    for triangle in range(15, 20):
                        for x in range(y, -1, -1):
                            node = self.mapper.get_node_by_xy(triangle, x,y)
                            lat = y
                            self.set_temperature(node, lat)
                            self.set_wind(node, (self.edge_length - y) + (2 * self.edge_length))
                            self.set_precipitation(node, (self.edge_length - y) + (2 * self.edge_length))

    def set_temperature(self, node, lat):
        # average temperature of a hex will be set to average of neighbors + a random value based on latitude.
        # This will result in average temperatures from -85 to 107 degrees fahrenheit.  This average temperature is then
        # reduced further based on elevation at a rate of -3 degrees per 1000 feet of elevation
        temp = (random.randrange(-85, -55) + (self.latitude_per_hex * lat * 1.9))
        ave = self.average_temperature(node.neighbors.values())
        if ave is not None:
            temp = (ave * 2 + temp)/3
        if node.get_attribute("height") > self.cold_height:
            node.set_attribute("temperature", temp - int(3 * ((node.get_attribute("height") - self.cold_height)/100)))
        else:
            node.set_attribute("temperature", temp)
        if node.get_attribute("temperature") < -35 and node.get_attribute("height") <= 0:
            node.set_attribute("height", 1)


    def average_temperature(self, nodes):
        """
        finds the average of a list of values
        ignores a value if it is null
        :param vales: a list of temperature values to be averaged
        """
        total = 0
        count = 0
        for node in nodes:
            if node.get_attribute("temperature") is not None:
                total += node.get_attribute("temperature")
                count += 1
        if count > 0:
            return total / count
        else:
            return None

    def average_precipitation(self, nodes):
        """
        finds the average of a list of values
        ignores a value if it is null
        :param vales: a list of temperature values to be averaged
        """
        total = 0
        count = 0
        for node in nodes:
            if node.get_attribute("precipitation") is not None:
                total += node.get_attribute("precipitation")
                count += 1
        if count > 0:
            return total / count
        else:
            return None

    def set_wind(self, node, lat):
        #Set wind direction of a node based on its latitude
        # 0 - Not Set
        # 1 - Northeast
        # 2 - East
        # 3 - Southeast
        # 4 - Southwest
        # 5 - West
        # 6 - Northwest
        if (self.latitude_per_hex * lat) < 10:
            node.set_attribute("wind", 5)
        elif (self.latitude_per_hex * lat) < 25:
            node.set_attribute("wind", 4)
        elif (self.latitude_per_hex * lat) < 30:
            node.set_attribute("wind",3)
        elif (self.latitude_per_hex * lat) < 40:
            node.set_attribute("wind", 2)
        elif (self.latitude_per_hex * lat) < 60:
            node.set_attribute("wind", 1)
        elif (self.latitude_per_hex * lat) < 65:
            node.set_attribute("wind", 6)
        elif (self.latitude_per_hex * lat) < 85:
            node.set_attribute("wind", 4)
        elif (self.latitude_per_hex * lat) < 95:
            node.set_attribute("wind", 5)
        elif (self.latitude_per_hex * lat) < 115:
            node.set_attribute("wind", 6)
        elif (self.latitude_per_hex * lat) < 120:
            node.set_attribute("wind", 4)
        elif (self.latitude_per_hex * lat) < 140:
            node.set_attribute("wind", 3)
        elif (self.latitude_per_hex * lat) < 150:
            node.set_attribute("wind", 2)
        elif (self.latitude_per_hex * lat) < 155:
            node.set_attribute("wind", 1)
        elif (self.latitude_per_hex * lat) < 170:
            node.set_attribute("wind", 6)
        else:
            node.set_attribute("wind", 5)

    def set_precipitation(self, node, lat):
        pass
        lat = (lat * self.latitude_per_hex)%60
        if lat > 30:
            lat = 30 - (lat-30)
        precipitation = (lat * 12) + random.randrange(10, 30)
        node.set_attribute("precipitation", precipitation)

    def simulate_wind(self, node, iterations):
        if node.get_attribute("height") > 0:
            starting_node = node
            precipitation = node.get_attribute("precipitation")
            steps = 0
            height = node.get_attribute("height")
            while steps < iterations:
                if node.get_attribute("height") <= 0 or node.get_attribute("river"):
                    precipitation += precipitation * .01
                if precipitation > node.get_attribute("precipitation"):
                    node.set_attribute("precipitation", node.get_attribute("precipitation") + (.02 * precipitation))
                    precipitation -= .02 * precipitation
                elif precipitation < node.get_attribute("precipitation"):
                    node.set_attribute("precipitation", node.get_attribute("precipitation") - (.02 * node.get_attribute("precipitation")))
                    precipitation += .02 * node.get_attribute("precipitation")
                if height < node.get_attribute("height"):
                    node.set_attribute("precipitation", node.get_attribute("precipitation") + (precipitation * ((node.get_attribute("height") - height)/10000)))
                    precipitation = precipitation - (precipitation * ((node.get_attribute("height") - height)/10000))
                height = node.get_attribute("height")
                if node.get_attribute("precipitation") < 10:
                    node.set_attribute("precipitation", 10)
                #figuring out wind direction
                if node.get_attribute("wind") is 1:
                    if "northeast" in node.neighbors:
                        if "northwest" in node.neighbors:
                            node.neighbors["northwest"].set_attribute("precipitation", node.neighbors["northwest"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        if "east" in node.neighbors:
                            node.neighbors["east"].set_attribute("precipitation", node.neighbors["east"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["northeast"]
                    elif "east" in node.neighbors:
                        if "southeast" in node.neighbors:
                            node.neighbors["southeast"].set_attribute("precipitation", node.neighbors["southeast"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["east"]
                    elif "northwest" in node.neighbors:
                        if "west" in node.neighbors:
                            node.neighbors["west"].set_attribute("precipitation", node.neighbors["west"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["northwest"]
                elif node.get_attribute("wind") is 2:
                    if "east" in node.neighbors:
                        if "northeast" in node.neighbors:
                            node.neighbors["northeast"].set_attribute("precipitation", node.neighbors["northeast"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        if "southeast" in node.neighbors:
                            node.neighbors["southeast"].set_attribute("precipitation", node.neighbors["southeast"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["east"]
                    elif"southeast" in node.neighbors:
                        if "southwest" in node.neighbors:
                            node.neighbors["southwest"].set_attribute("precipitation", node.neighbors["southwest"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["southeast"]
                    elif "northeast" in node.neighbors:
                        if "northwest" in node.neighbors:
                            node.neighbors["northwest"].set_attribute("precipitation", node.neighbors["northwest"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["northeast"]
                elif node.get_attribute("wind") is 3:
                    if "southeast" in node.neighbors:
                        if "east" in node.neighbors:
                            node.neighbors["east"].set_attribute("precipitation", node.neighbors["east"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        if "southwest" in node.neighbors:
                            node.neighbors["southwest"].set_attribute("precipitation", node.neighbors["southwest"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["southeast"]
                    elif "east" in node.neighbors:
                        if "northeast" in node.neighbors:
                            node.neighbors["northeast"].set_attribute("precipitation", node.neighbors["northeast"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["east"]
                    elif "southwest" in node.neighbors:
                        if "west" in node.neighbors:
                            node.neighbors["west"].set_attribute("precipitation", node.neighbors["west"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["southwest"]
                elif node.get_attribute("wind") is 4:
                    if "southwest" in node.neighbors:
                        if "southeast" in node.neighbors:
                            node.neighbors["southeast"].set_attribute("precipitation", node.neighbors["southeast"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        if "west" in node.neighbors:
                            node.neighbors["west"].set_attribute("precipitation", node.neighbors["west"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["southwest"]
                    elif "southeast" in node.neighbors:
                        if "east" in node.neighbors:
                            node.neighbors["east"].set_attribute("precipitation", node.neighbors["east"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["southeast"]
                    elif "west" in node.neighbors:
                        if "northwest" in node.neighbors:
                            node.neighbors["northwest"].set_attribute("precipitation", node.neighbors["northwest"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["west"]
                elif node.get_attribute("wind") is 5:
                    if "west" in node.neighbors:
                        if "southwest" in node.neighbors:
                            node.neighbors["southwest"].set_attribute("precipitation", node.neighbors["southwest"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        if "northwest" in node.neighbors:
                            node.neighbors["northwest"].set_attribute("precipitation", node.neighbors["northwest"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["west"]
                    elif "southwest" in node.neighbors:
                        if "southeast" in node.neighbors:
                            node.neighbors["southeast"].set_attribute("precipitation", node.neighbors["southeast"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["southwest"]
                    elif"northwest" in node.neighbors:
                        if "northeast" in node.neighbors:
                            node.neighbors["northeast"].set_attribute("precipitation", node.neighbors["northeast"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["northwest"]
                elif node.get_attribute("wind") is 6:
                    if "norhtwest" in node.neighbors:
                        if "northeast" in node.neighbors:
                            node.neighbors["northeast"].set_attribute("precipitation", node.neighbors["northeast"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        if "west" in node.neighbors:
                            node.neighbors["west"].set_attribute("precipitation", node.neighbors["west"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["northwest"]
                    elif "west" in node.neighbors:
                        if "southwest" in node.neighbors:
                            node.neighbors["southwest"].set_attribute("precipitation", node.neighbors["southwest"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["west"]
                    elif "northeast" in node.neighbors:
                        if "east" in node.neighbors:
                            node.neighbors["east"].set_attribute("precipitation", node.neighbors["east"].get_attribute("precipitation") + (.02 * precipitation))
                            precipitation -= .02 * precipitation
                        node = node.neighbors["northeast"]
                if node is starting_node:
                    break
                steps += 1