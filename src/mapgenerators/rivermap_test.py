import unittest
import random
from src.mapgenerators.rivermap import RiverMap
from src.mapgenerators.heightmap import HeightMap
from src.nodemapper.node_mapper import NodeMapper

class TestRiverMap(unittest.TestCase):

    def setUp(self):
        self.number_of_triangles = 1
        self.rivers_per_triangle = 7
        self.mountain_height = 500

    def test_set_sources(self):
        #Unfortunately we need a hight map to do this but I will see the hightmap to 1313 so it is always the same map
        self.mapper = NodeMapper()
        self.mapper.make_triangle(0)
        self.height_map = HeightMap(self.mapper, seed_value=1313)
        for i in range(0, self.number_of_triangles):
            self.height_map.build_height_map(i)
        self.river_map = RiverMap(self.mapper, number_of_rivers=self.number_of_triangles * self.rivers_per_triangle, seed=7171, mountain_height=self.mountain_height)
        self.river_map.set_sources()

        #set_sources should spawn a set numer of rivers which are stored in active rivers
        self.assertEqual(self.river_map.number_of_rivers * 20, len(self.river_map.active_rivers))
        #all rivers should start in the mountains and therefore have a height greater than the mountain height
        for node in self.river_map.active_rivers:
            self.assertGreaterEqual(node.get_attribute('height'), self.river_map.mountain_height)

    def test_build_rivers(self):
        #Unfortunately we need a hight map to do this but I will see the hightmap to 1313 so it is always the same map
        self.mapper = NodeMapper()
        self.mapper.make_triangle(0)
        self.height_map = HeightMap(self.mapper, seed_value=1313)
        for i in range(0, self.number_of_triangles):
            self.height_map.build_height_map(i)
        self.river_map = RiverMap(self.mapper, number_of_rivers=self.number_of_triangles * self.rivers_per_triangle, seed=7171, mountain_height=self.mountain_height)
        self.river_map.set_sources()

        self.river_map.build_rivers()

        #When build_rivers is called all active rivers should be removed
        self.assertEqual(0, len(self.river_map.active_rivers))
        #TODO think of a more robust way of testing build_rivers functionality
