import random
import heapq

class RiverMap(object):

    def __init__(self, node_mapper, number_of_rivers=7, seed=None, mountain_height=200):
        self.active_rivers = []
        self.node_mapper = node_mapper
        self.number_of_rivers = number_of_rivers
        if seed is not None:
            random.seed = seed
        self.mountain_height = mountain_height

    def set_sources(self):
        self.active_rivers = random.sample(set([i for i in self.node_mapper.nodes.values() if i.get_attribute('height') >= self.mountain_height]), self.number_of_rivers * 100)

    def build_rivers(self):
        counter = 0
        for node in self.active_rivers:
            #working_node is used to move from one node to the next
            working_node = node
            #we need to have list of previous nodes to ensure we don't wrap on ourself
            previous_nodes = [node]
            #working_node is none when we have reached a terminating point
            while working_node is not None:
                #print("CURRENT HEIGHT: " + str(working_node.get_attribute('height')))
                lowest_neighbor = None
                #look at all neighbors and find the lowest point
                for neighbor in working_node.neighbors.values():
                    if neighbor not in previous_nodes and ((lowest_neighbor is not None and neighbor.get_attribute('height') < lowest_neighbor.get_attribute('height')) or (lowest_neighbor is None)):
                        #print("      NEIGHBOR HEIGHT: " + str(neighbor.get_attribute('height')))
                        lowest_neighbor = neighbor
                #check to see if the lowest neighbor is low enough for river to flow, if not the river terminates in a lake
                if lowest_neighbor is not None and lowest_neighbor.get_attribute('height') - 10 < working_node.get_attribute('height'):
                    #if the lowest node is higher than the working node but within terraforming bounds then we lower the lowest node to just lower than the working node
                    if lowest_neighbor.get_attribute('height') > working_node.get_attribute('height'):
                        lowest_neighbor.set_attribute('height', working_node.get_attribute('height') - 1)
                    #if the lowest node is below sea level then we are done
                    if lowest_neighbor.get_attribute('height') <= 0:
                        working_node = None
                        lowest_neighbor.set_attribute('river', True)
                        for rive_node in previous_nodes:
                            rive_node.set_attribute('river', True)
                        counter += 1
                    #if the lowest node is already a river we are done but will need to connect the two rivers
                    elif lowest_neighbor.get_attribute('river'):
                        working_node = None
                        if len(previous_nodes) > 5:
                            lowest_neighbor.set_attribute('river', True)
                            for rive_node in previous_nodes:
                                rive_node.set_attribute('river', True)
                            counter += 1
                    else:
                        previous_nodes.append(lowest_neighbor)
                        working_node = lowest_neighbor
                    #print("      SELECTED HEIGHT: " + str(lowest_neighbor.get_attribute('height')))
                else:
                    #print("River ENDS")
                    working_node = None
            previous_nodes.clear()
            if counter >= self.number_of_rivers:
                break
        print("TOTAL RIVERS: " + str(counter))
        self.active_rivers.clear()