import random
from random import seed
import math
import heapq

class HeightMap(object):

    #A value that sets the magnitue for the random height variance.  The larger the value the more jagged the terrain will be
    roughness = 8

    triangle = None
    array_size = None
    size = None

    def __init__(self, node_map, size=513, seed_value=None):
        self.node_map = node_map
        self.size = size
        self.array_size = size - 1
        self.values = None
        self.sea_level = 0
        self.height_max = 0
        self.height_min = 0
        if seed_value is not None:
            seed(seed_value)

    def set_node(self, x, y, value):
        """
        sets the height of the specified cell if the value has not already been set
        Will be ignored if the height is not null
        :param x: the x coordinate
        :param y: the y coordinate
        :param value: the height value for the node
        :return:
        """
        try:
            self.node_map.get_node_by_xy(self.triangle, x, y).set_attribute('height', int(value))
        except BaseException as e:
            return False

    def get_node(self, x, y):
        """
        gets the height of the specified cell
        :param x: the x coordinate
        :param y: the y coordinate
        """
        try:
            return self.node_map.get_node_by_xy(self.triangle, x, y).get_attribute('height')
        except Exception:
            return None

    def average(self, values):
        """
        finds the average of a list of values
        ignores a value if it is null
        :param vales: a list of height values to be averaged
        """
        try:
            total = 0
            count = 0
            for value in values:
                if value is not None:
                    total += value
                    count += 1
            return total / count
        except BaseException as e:
            return None

    def set_corners(self, seed_value=None):
        """
        sets the 3 corners of the triangle to random values between -250 and 250
        :param seed: sets the random number generator to a given seed for replicable results
        """
        if seed_value is not None:
            random.seed = seed_value
        if self.node_map.get_node_by_xy(self.triangle, 0, 0).get_attribute('height') is None:
            self.set_node(0, 0, random.randrange(-250, 250))
        if self.node_map.get_node_by_xy(self.triangle, 0, self.array_size).get_attribute('height') is None:
            self.set_node(0, self.array_size, random.randrange(-250, 250))
        if self.node_map.get_node_by_xy(self.triangle, self.array_size, self.array_size).get_attribute('height') is None:
            self.set_node(self.array_size, self.array_size, random.randrange(-250, 250))

    def set_edge(self, x, y, size, offset, orientation, face):
        """
        sets the midpoint of an edge of the triangle to a the average of the endpoints and left and right center point plus or minus an offset
        :param x: the x coordinate of the edge midpoint
        :param y: the y coordinate of the edge midpoint
        :param size: the size of the triagle we are currently working on.  Gets smaller with recursion
        :param offset: a random value scaled by the ROUGHNESS variable and size
        :param orientation: a variable to dictate if we are looking at the left or right edge
        :param face: a variable to dictate if we are wooking with a left/right edge or a bottom face
        """
        ave = None
        if face is 'EDGE':
            if orientation is 'LEFT':
                ave = self.average({
                    self.get_node(x, y - size),  # upper point
                    self.get_node(x, y + size),  # lower point
                    self.get_node(x - int(size / 2), y),  # left center
                    self.get_node(x + int(size / 2), y),  # right center
                })
            else:
                ave = self.average({
                    self.get_node(x - size, y - size),  # upper point
                    self.get_node(x + size, y + size),  # lower point
                    self.get_node(x - int(size / 2), y),  # left center
                    self.get_node(x + int(size / 2), y),  # right center
                })
        else:
            ave = self.average({
                self.get_node(x - int(size / 2), y - size),  # upper center
                self.get_node(x + int(size / 2), y + size),  # lower center
                self.get_node(x - size, y),  # left point
                self.get_node(x + size, y),  # right point
            })
        self.set_node(x, y, ave + offset)

    def set_center(self, x, y, size, offset, orientation):
        """
        sets the center of the triangle to a the average of the 3 corners of the triangle
        :param x: the x coordinate of the edge midpoint
        :param y: the y coordinate of the edge midpoint
        :param size: the size of the triagle we are currently working on.  Gets smaller with recursion
        :param offset: a random value scaled by the ROUGHNESS variable and size
        :param orientation: a variable to dictate if the triangle is pointside up or pointside down
        """
        ave = None
        if orientation is 'UP':
            self.values = {self.get_node(x - int(size / 2), y - size),  # upper center
                           self.get_node(x - int(size / 2), y + size),  # lower left
                           self.get_node(x + int(size / 2) + size, y + size)}  # lower right
            ave = self.average(self.values)
            self.set_node(x, y, ave + offset)
        else:
            self.values = {self.get_node(x - size - int(size / 2), y - size),  # upper left
                           self.get_node(x + int(size / 2), y - size),  # upper right
                           self.get_node(x + int(size / 2), y + size)}  # lower center
            ave = self.average(self.values)
            self.set_node(x, y, ave + offset)

    def divide(self, size):
        """
        This is a modified Diamond-square algorithm https://en.wikipedia.org/wiki/Diamond-square_algorithm
        1. The alogirthm works on triangles of size 2^n + 1 and it assumes the corners of the triangles are set to random values
        2. The algorith sets the center and the midpoints of the edges to an average plus a random offset
        3. This creates 4 smaller triangle (think triforce)
        4. The algorithm then runs on the 4 smaller triangles
        5. Once the algorithm has run across all triangles of side length 3 the algorithm terminates since all values in the tringle have been set
        :param size: the 0 indexed length of a triangle side (length of a side - 1)
        """
        half = int(size / 2)
        scale = self.roughness * size
        if half < 1:
            return True
        if half > 1:
            y = half
            while y < self.array_size:
                x = int(half / 2)
                orientation = 'UP'
                while x < y:
                    if self.node_map.get_node_by_xy(self.triangle, x, y).get_attribute('height') is None:
                        self.set_center(x, y, half, random.random() * scale * 2 - scale, orientation)
                    if orientation is 'UP':
                        orientation = 'DOWN'
                    else:
                        orientation = 'UP'
                    x += int(half)
                y += size
        y = half
        face = 'EDGE'
        while y <= self.array_size:
            x = 0
            orientation = 'LEFT'
            while x <= y:
                if self.node_map.get_node_by_xy(self.triangle, x, y).get_attribute('height') is None:
                    self.set_edge(x, y, half, random.random() * scale * 2 - scale, orientation, face)
                if orientation is 'LEFT':
                    orientation = 'RIGHT'
                else:
                    orientation = 'LEFT'
                x += half
            if face is 'EDGE':
                face = 'BOTTOM'
            else:
                face = 'EDGE'
            y += half
        self.divide(half)

    def build_height_map(self, triangle=0):
        self.triangle = triangle
        self.set_corners()
        self.divide(self.array_size)
        self.priority_flood()

    def make_world(self, number_of_triangles):
        for i in range(0, number_of_triangles):
            self.node_map.make_triangle(i)
            if i is 0:
                pass
            elif i < 5:
                self.node_map.strip_right_to_left(i - 1, i)
                if i is 4:
                    self.node_map.strip_left_to_right(0, i)
            elif i is 5:
                self.node_map.strip_bottom_to_bottom(0, i)
            elif i < 15:
                if i % 2 is 0:
                    self.node_map.strip_uright_to_left(i - 1, i)
                    if i is 14:
                        self.node_map.strip_uleft_to_right(5, 14)
                else:
                    self.node_map.strip_bottom_to_bottom((i - 5)/2, i)
                    self.node_map.strip_right_to_uleft(i - 1, i)
            else:
                self.node_map.strip_bottom_to_bottom((i - 12) * 2, i)
                if(i > 15):
                    self.node_map.strip_left_to_right(i - 1, i)
                if i is 19:
                    self.node_map.strip_right_to_left(15, i)
            self.build_height_map(i)
            self.node_map.introduce_neighbors(i)

    def get_stats(self):
        direction_neighbors = 0
        other_neighbors = 0
        five_neighbors = 0
        six_neighbors = 0
        else_neighbors = 0
        for node in set(self.node_map.nodes.values()):
            if len(node.neighbors) is 12:
                six_neighbors += 1
            elif len(node.neighbors) is 10:
                five_neighbors += 1
            else:
                else_neighbors += 1
            if self.height_max < node.get_attribute('height'):
                self.height_max = node.get_attribute('height')
            if self.height_min > node.get_attribute('height'):
                self.height_min = node.get_attribute('height')
        print("Max: " + str(self.height_max))
        print("Min: " + str(self.height_min))
        print("12 Neighbors: " + str(six_neighbors))
        print("10 Neighbors: " + str(five_neighbors))
        print("else Neighbors: " + str(else_neighbors))

    def set_sea_level(self, land_percentage):
        self.sea_level = heapq.nlargest(math.floor(self.node_map.node_count() * land_percentage), self.node_map.nodes.values(), key= lambda  o: o.get_attribute('height'))[math.floor(self.node_map.node_count() * land_percentage) - 1].get_attribute('height')
        print("Sea Level: " + str(self.sea_level))
        for node in set(self.node_map.nodes.values()):
            node.set_attribute('height', node.get_attribute('height') - self.sea_level)

    def average_height(self, nodes):
        """
        finds the average of a list of values
        ignores a value if it is null
        :param vales: a list of temperature values to be averaged
        """
        total = 0
        count = 0
        for node in nodes:
            if node.get_attribute("height") is not None:
                total += node.get_attribute("height")
                count += 1
        return total / count

    def priority_flood(self):
        """
        this algorithm takes a heightmap and will smooth it so that every cell has a neighbor that is lower than it
        """
        self.node_map