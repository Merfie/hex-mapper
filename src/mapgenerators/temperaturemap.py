import random
import math

class TemperatureMap(object):

    def __init__(self, node_map, max_height, size=513, seed_value=None, ):
        self.node_map = node_map
        self.triangle_size = size
        self.seed = seed_value
        self.size = size
        self.full_zize = 1.5 * (size - 1)
        self.min_temp = float("inf")
        self.max_temp = float("-inf")
        self.max_height = max_height

    def set_temps(self):
        for i in range(0, 20):
            for y in range(0, self.size):
               for x in range(0, y + 1):
                    node = self.node_map.get_node_by_xy(i, x, y)
                    lat = 0
                    if i < 5 or i > 14:
                        lat = y
                    else:
                        lat = self.triangle_size + y
                        if lat > self.full_zize:
                            lat = self.triangle_size * 2 - y
                    temp = self.get_base_latatude_temp(i, lat)
                    if node.get_attribute("height") > 0:
                        lat_degree = (1 - (lat/self.full_zize)) * 90
                        temp -= ((node.get_attribute("height") * 10)/((59000 - (566.6667 * lat_degree) + 2.2222*lat_degree*lat_degree) * (self.size/513))) * (temp + 70)
                    if temp < -70:
                        temp = -70
                    temp *= (random.randrange(95, 105) / 100)
                    node.set_attribute("temperature", temp)
                    if temp > self.max_temp:
                        self.max_temp = temp
                    if temp < self.min_temp:
                        self.min_temp = temp

    def get_base_latatude_temp(self, t, lat):
        lat_temp = 0
        lat_temp = 150 * (lat/self.full_zize) - 55
        return lat_temp


