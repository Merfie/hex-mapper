import unittest
from src.mapgenerators.heightmap import HeightMap
from src.nodemapper.node_mapper import NodeMapper
from src.mapgenerators.biomemap import BiomeMap


class TestBioMap(unittest.TestCase):
    def test_average_temperature(self):
        bio_mapper = BiomeMap()
        bio_mapper.average_temperature()