import unittest
from src.mapgenerators.heightmap import HeightMap
from src.nodemapper.node_mapper import NodeMapper


class TestHeightMap(unittest.TestCase):

    def test_getandset_node(self):
        self.mapper = NodeMapper(triangle_edge=17)
        self.mapper.make_triangle(0)
        self.height_map = HeightMap(self.mapper, size=17)
        self.height_map.triangle = 0
        #Test None
        self.assertIsNone(self.height_map.get_node(4, 9))
        #Test Set
        self.height_map.set_node(4, 9, 22)
        self.assertEqual(22, self.height_map.get_node(4, 9))

    def test_average(self):
        self.mapper = NodeMapper(triangle_edge=17)
        self.mapper.make_triangle(0)
        self.height_map = HeightMap(self.mapper, size=17)
        #Test base average
        ave = self.height_map.average((12, 12, 12))
        self.assertEqual(12, ave)
        #Test ignore None
        ave = self.height_map.average((20, None, None, 20, None))
        self.assertEqual(20, ave)
        #Test real average
        ave = self.height_map.average((10, 20, 30))
        self.assertEqual(20, ave)
        #Test Error
        ave = self.height_map.average((None, None, None))
        self.assertIsNone(ave)

    def test_set_corners(self):
        self.mapper = NodeMapper(triangle_edge=17)
        self.mapper.make_triangle(0)
        self.height_map = HeightMap(self.mapper, size=17)
        self.height_map.triangle = 0
        self.assertIsNone(self.height_map.get_node(0,0))
        self.assertIsNone(self.height_map.get_node(16,16))
        self.assertIsNone(self.height_map.get_node(0,16))
        #set rng to known seed to check know results
        self.height_map.set_corners(seed=112233)
        self.assertEqual(-211, self.height_map.get_node(0,0))
        self.assertEqual(163, self.height_map.get_node(0,16))
        self.assertEqual(211, self.height_map.get_node(16,16))
        #check for only 3 nodes set
        self.count = 0
        for node in self.mapper.get_triangle(0):
            if node.get_attribute('height') is not None:
                self.count += 1
        self.assertEqual(3, self.count)

    def test_set_center(self):
        self.mapper = NodeMapper(triangle_edge=17)
        self.mapper.make_triangle(0)
        self.height_map = HeightMap(self.mapper, size=17)
        self.height_map.triangle = 0
        #Set some static values
        self.height_map.set_node(0, 0, 123)
        self.height_map.set_node(0, 16, 22)
        self.height_map.set_node(16, 16, -48)
        self.height_map.set_node(0, 8, 444)
        self.height_map.set_node(8, 8, 111)
        self.height_map.set_node(8, 16, -222)
        #Test upward center
        self.height_map.set_center(4, 8, 8, 66, 'UP')
        self.assertEqual(98, self.height_map.get_node(4, 8))
        #center should always find values for all points
        self.assertEqual(3, sum(x is not None for x in self.height_map.values))
        #Test downward center
        self.height_map.set_center(6, 12, 4, 77, 'DOWN')
        self.assertEqual(188, self.height_map.get_node(6, 12))
        #center should always find values for all points in center
        self.assertEqual(3, sum(x is not None for x in self.height_map.values))

    def test_set_edge(self):
        self.mapper = NodeMapper(triangle_edge=17)
        self.mapper.make_triangle(0)
        self.height_map = HeightMap(self.mapper, size=17)
        self.height_map.triangle = 0
        #Set some static values
        self.height_map.set_node(0, 0, 123)
        self.height_map.set_node(0, 16, 22)
        self.height_map.set_node(16, 16, -48)
        self.height_map.set_node(4, 8, 55)
        #Test Left Edge
        self.height_map.set_edge(0, 8, 8, 66, 'LEFT', 'EDGE')
        self.assertEqual(132, self.height_map.get_node(0, 8))
        #Test Right Edge
        self.height_map.set_edge(8, 8, 8, 66, 'RIGHT', 'EDGE')
        self.assertEqual(109, self.height_map.get_node(8, 8))
        #Test Bottom Edge
        self.height_map.set_edge(8, 16, 8, 66, 'DOESNT_MATTER', 'BOTTOM')
        self.assertEqual(75, self.height_map.get_node(8, 16))

    def test_divide(self):
        self.mapper = NodeMapper(triangle_edge=17)
        self.mapper.make_triangle(0)
        self.height_map = HeightMap(self.mapper, size=17)
        self.height_map.triangle = 0
        #Set corner values
        self.height_map.set_node(0,0, 200)
        self.height_map.set_node(0,16, -150)
        self.height_map.set_node(16,16, 52)
        #make sure divide sets all node
        self.height_map.divide(16)
        self.assertEqual(0, sum(x is None for x in self.mapper.get_triangle(0)))

    def test_set_sea_level(self):
        #Not sure what to test hear since it requires a complete height map and therefore relies on all other methods working correctly
        pass





