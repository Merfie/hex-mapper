import unittest
from src.nodemapper.node_mapper import NodeMapper


class NodeMapperTest(unittest.TestCase):
    def setUp(self):
        self.mapper = NodeMapper(5)
        self.mapper.make_triangle(0)

    def test_create_triangle_array(self):
        self.mapper.make_triangle(0)
        self.assertEqual(15, self.mapper.node_count())
        self.assertIsNotNone(self.mapper.get_node(0, 0, 0))
        self.assertIsNotNone(self.mapper.get_node(0, 4, 0))
        self.assertIsNotNone(self.mapper.get_node(0, 0, 4))
        self.assertIsNotNone(self.mapper.get_node(0, 1, 1))
        self.assertIsNone(self.mapper.get_node(0, 0, 5))
        self.assertIsNone(self.mapper.get_node(0, 5, 0))

    def test_get_node_by_xy(self):
        self.assertEqual(15, self.mapper.node_count())
        qr_node0 = self.mapper.get_node(0, 0, 0)
        node_0_height = 20
        qr_node0.set_attribute("height", node_0_height)
        self.assertEqual(node_0_height, self.mapper.get_node_by_xy(0, 0, 0).get_attribute("height"))

        qr_node_1 = self.mapper.get_node(0, 0, 4)
        node_1_height = 40
        qr_node_1.set_attribute("height", node_1_height)
        self.assertEqual(node_1_height, self.mapper.get_node_by_xy(0, 0, 4).get_attribute("height"))

        qr_node_2 = self.mapper.get_node(0, 4, 0)
        node_2_height = 60
        qr_node_2.set_attribute("height", node_2_height)
        self.assertEqual(node_2_height, self.mapper.get_node_by_xy(0, 4, 4).get_attribute("height"))

        qr_node_3 = self.mapper.get_node(0, 1, 0)
        node_3_height = 80
        qr_node_3.set_attribute("height", node_3_height)
        self.assertEqual(node_3_height, self.mapper.get_node_by_xy(0, 1, 1).get_attribute("height"))
        self.assertIsNone(self.mapper.get_node_by_xy(0, 0, 5))
        self.assertIsNone(self.mapper.get_node_by_xy(0, 5, 0))

    def test_get_two_triangles(self):
        self.mapper.make_triangle(1)
        valid_points = {(0, q, r) for q in range(5) for r in range(5) if q + r < 5}
        self.mapper.get_triangle(0)
        self.assertEqual(15, self.mapper.get_triangle(0).__len__())
        self.assertEqual({x.get_coordinates_for_triangle(0) for x in self.mapper.get_triangle(0)}, valid_points)

    def test_update_triangle(self):
        for q, r in self.mapper.triangle_coordinates_iterator():
            self.mapper.get_node(0, q, r).set_attribute("height", 10 + q + r)

        for q, r in self.mapper.triangle_coordinates_iterator():
            self.assertEqual(self.mapper.get_node(0, q, r).get_attribute("height"), 10 + q + r)

    def test_update_many_triangles(self):
        nodes = self.mapper.get_triangle(0)
        for node in nodes:
            node.set_attribute("height", 10 + node.get_coordinates_for_triangle(0)[1] +
                               node.get_coordinates_for_triangle(0)[2])

        for q, r in self.mapper.triangle_coordinates_iterator():
            self.assertEqual(self.mapper.get_node(0, q, r).get_attribute("height"), 10 + q + r)

        self.assertEqual(self.mapper.node_count(), 15)

        # TODO offshore a lot of the triangle operations on to the mapper and make them take lists

    def test_corners_are_pentagons(self):
        self.assertEqual(self.mapper.get_node(0, 0, 0).max_neighbors, 5)
        self.assertEqual(self.mapper.get_node(0, 0, 4).max_neighbors, 5)
        self.assertEqual(self.mapper.get_node(0, 4, 0).max_neighbors, 5)

    def test_set_corner_node_neighbors(self):
        self.mapper.introduce_neighbors(0)

        top_node = self.mapper.get_node(0, *self.mapper.top_corner_qr)
        self.assertIsNotNone(top_node.get_q_neighbor(0))
        self.assertTrue((0, 1, 0) in top_node.get_q_neighbor(0))
        self.assertIsNotNone(top_node.get_r_neighbor(0))
        self.assertTrue((0, 0, 1) in top_node.get_r_neighbor(0))
        self.assertEqual(self.mapper.get_node_q_neighbor(0, *self.mapper.top_corner_qr), top_node.get_q_neighbor(0))
        self.assertEqual(self.mapper.get_node_r_neighbor(0, *self.mapper.top_corner_qr), top_node.get_r_neighbor(0))

        left_node = self.mapper.get_node(0, *self.mapper.left_corner_qr)
        self.assertIsNotNone(left_node.get_q_neighbor(0))
        self.assertTrue((0, 1, 3) in left_node.get_q_neighbor(0))
        self.assertIsNotNone(left_node.get_r_neighbor(0))
        self.assertTrue((0, 0, 3) in left_node.get_r_neighbor(0))
        self.assertEqual(self.mapper.get_node_r_neighbor(0, *self.mapper.left_corner_qr), left_node.get_r_neighbor(0))
        self.assertEqual(self.mapper.get_node_q_neighbor(0, *self.mapper.left_corner_qr), left_node.get_q_neighbor(0))

        right_node = self.mapper.get_node(0, *self.mapper.right_corner_qr)
        self.assertIsNotNone(right_node.get_q_neighbor(0))
        self.assertTrue((0, 3, 0) in right_node.get_q_neighbor(0))
        self.assertIsNotNone(right_node.get_r_neighbor(0))
        self.assertTrue((0, 3, 1) in right_node.get_r_neighbor(0))
        self.assertEqual(self.mapper.get_node_r_neighbor(0, *self.mapper.right_corner_qr), right_node.get_r_neighbor(0))
        self.assertEqual(self.mapper.get_node_q_neighbor(0, *self.mapper.right_corner_qr), right_node.get_q_neighbor(0))

        self.assertIsNone(self.mapper.get_node(0, 0, 0).get_q_neighbor(1))

    def test_strip_right_to_left(self):
        self.mapper.introduce_neighbors(0)
        self.mapper.make_triangle(1)
        self.mapper.strip_right_to_left(0, 1)
        self.assertEqual(self.mapper.get_node(0, 0, 0), self.mapper.get_node(1, 0, 0))
        self.assertEqual(self.mapper.get_node(0, 2, 0), self.mapper.get_node(1, 0, 2))

    def test_set_hex_node_neighbors(self):
        self.mapper.introduce_neighbors(0)

        middle_node = self.mapper.get_node(0, 1, 1)
        self.assertEqual((0, 1, 0), middle_node.get_hex_neighbor_in_triangle(0, 0))
        self.assertEqual((0, 2, 0), middle_node.get_hex_neighbor_in_triangle(1, 0))
        self.assertEqual((0, 2, 1), middle_node.get_hex_neighbor_in_triangle(2, 0))
        self.assertEqual((0, 1, 2), middle_node.get_hex_neighbor_in_triangle(3, 0))
        self.assertEqual((0, 0, 2), middle_node.get_hex_neighbor_in_triangle(4, 0))
        self.assertEqual((0, 0, 1), middle_node.get_hex_neighbor_in_triangle(5, 0))

        edge_node = self.mapper.get_node(0, 1, 0)
        self.assertIsNone(edge_node.get_hex_neighbor_in_triangle(0, 0))
        self.assertIsNone(edge_node.get_hex_neighbor_in_triangle(1, 0))
        self.assertEqual((0, 2, 0), edge_node.get_hex_neighbor_in_triangle(2, 0))
        self.assertEqual((0, 1, 1), edge_node.get_hex_neighbor_in_triangle(3, 0))
        self.assertEqual((0, 0, 1), edge_node.get_hex_neighbor_in_triangle(4, 0))
        self.assertEqual((0, 0, 0), edge_node.get_hex_neighbor_in_triangle(5, 0))

    def test_get_node_by_multiple_identities(self):
        self.mapper.update_coordinates((0, 0, 0), (1, 0, 0))

        self.assertIsNotNone(self.mapper.get_node(1, 0, 0))

    @unittest.skip("This is a resource test. I should add really good logging")
    def test_too_many_cooks(self):
        self.mapper = NodeMapper()
        print('Seeding Map')
        for x in range(20):
            self.mapper.make_triangle(x)
            self.mapper.introduce_neighbors(x)

        print("Done populating")

    @unittest.skip("Run this occasionally to test runtime")
    def test_large_triangle(self):
        self.mapper = NodeMapper()
        print('Creating Triangle with side of 513')
        self.mapper.make_triangle(0)
        self.mapper.introduce_neighbors(0)

if __name__ == '__main__':
    unittest.main()
