from src.node.node import Node, neighbor_lot, left_neighbor_qr, right_neighbor_qr, top_neighbor_qr, hex_neighbor_qr
import random


class NodeMapper(object):
    """
        An object to create a node map and then perform useful stuff on it
    """

    def __init__(self, triangle_edge=513):
        """
        Constructor
        :param triangle_edge: length of triangle edge. defaults to 513
        """
        self.triangle_edge_length = triangle_edge
        self.nodes = {}
        self.top_corner_qr = (0, 0)
        self.left_corner_qr = (0, triangle_edge - 1)
        self.right_corner_qr = (triangle_edge - 1, 0)
        self.corners = {self.top_corner_qr, self.left_corner_qr, self.right_corner_qr}

    def node_count(self):
        return self.nodes.__len__()

    def make_triangle(self, triangle):
        """
        Create a triangle and add it to the nodemap
        :param triangle: the triangle key
        """

        # Create the node triangles
        for q, r in self.triangle_coordinates_iterator():
            tqr = (triangle, q, r)
            if (q, r) in self.corners:
                node = Node(5)
            else:
                node = Node()
            node.add_coordinates(*tqr)
            self.add_node(tqr, node)

        #print("Nodes Created")

    def introduce_neighbors(self, triangle):
        #print("Introducing Neighbors")

        for q, r in self.triangle_coordinates_iterator():
            tqr = (triangle, q, r)
            node = self.get_node(*tqr)
            # print("Examining node {}".format(tqr))
            # TODO implement competent logging

            if (q, r) in self.corners:
                #print("Corner found. Now Be Friends")
                if self.get_node_q_neighbor(*tqr) not in node.neighbors.values():
                    node.add_q_neighbor(self.get_node_q_neighbor(*tqr), tqr)
                if self.get_node_r_neighbor(*tqr) not in node.neighbors.values():
                    node.add_r_neighbor(self.get_node_r_neighbor(*tqr), tqr)

                continue
            else:
                # hex node
                for index, shift in hex_neighbor_qr.items():
                    neighbor_tqr = neighbor_lot(tqr, shift)
                    neighbor_node = self.get_node(*neighbor_tqr)
                    if neighbor_tqr not in node.neighbors and neighbor_node is not None and neighbor_node not in node.neighbors.values():
                        node.add_neighbor_by_coordinate(neighbor_node, neighbor_tqr)

    def get_node(self, triangle, q, r):
        """
        Get a node from the map that has the given coordinetes
        :param triangle: the triangle the node is in
        :param q: the q coordinate
        :param r: the r coordinate
        :return: a Node with the requested coordinates
        """
        try:
            # return next(filter(lambda x: (triangle, q, r) in x, self.nodes))
            return self.nodes[(triangle, q, r)]
        except KeyError:
            return None

    def get_node_by_xy(self, triangle, x, y):
        """
        Convert the x y coordinates to q r and then get a node from the map that has the given coordinetes
        :param triangle: the triangle the node is in
        :param x: the x coordinate
        :param y: the y coordinate
        :return: a Node with the requested coordinates
        """
        try:
            q = x
            r = y - x
            return self.get_node(triangle, q, r)
        except KeyError:
            return None

    def get_triangle(self, triangle):
        """
        Get a list of nodes that represents a triangle
        :param triangle: the triangle key you want to get
        :return: A List of nodes that represents a triangle tile
        """
        return {node for node in self.nodes.values() if node.is_triangle(triangle)}

    def add_node(self, tqr, node):
        """
        adds a node to the map. Let the make_triangle method use this
        :param node: the node to add to the bap
        """
        self.nodes[tqr] = node

    def triangle_coordinates_iterator(self):
        """
        get a coordinates iterator for a triangle
        :return: a set of tuples for iterating over the nodeset
        """
        result = []
        for q in range(self.triangle_edge_length):
            for r in range(self.triangle_edge_length - q):
                yield q, r

    def get_node_q_neighbor(self, t, q, r):
        corner_node = self.validate_corner(t, q, r)

        q_node = self.get_node(*neighbor_lot((t, q, r), corner_node.corner_neighbor_coordinates_for_tqr((t, q, r)).get("q")))

        if q_node is None:
            raise ValueError("That neighbor does not exist")
        return q_node

    def get_node_r_neighbor(self, t, q, r):
        corner_node = self.validate_corner(t, q, r)

        r_node = self.get_node(*neighbor_lot((t, q, r), corner_node.corner_neighbor_coordinates_for_tqr((t, q, r)).get("r")))

        if r_node is None:
            raise ValueError("That neighbor does not exist")
        return r_node

    def strip_right_to_left(self, source, destination):
        for i in range(0, self.triangle_edge_length):
            self.update_coordinates((source, i, 0), (destination, 0, i))

    def strip_left_to_right(self, source, destination):
        for i in range(0, self.triangle_edge_length):
            self.update_coordinates((source, 0, i), (destination, i, 0))

    def strip_bottom_to_bottom(self, source, destination):
        for i in range(0, self.triangle_edge_length):
            self.update_coordinates((source, i, self.triangle_edge_length - 1 - i), (destination, self.triangle_edge_length - 1 - i, i))

    def strip_uright_to_left(self, source, destination):
        for i in range(0, self.triangle_edge_length):
            self.update_coordinates((source, 0, i), (destination, 0, self.triangle_edge_length - 1 - i))

    def strip_uleft_to_right(self, source, destination):
        for i in range(0, self.triangle_edge_length):
            self.update_coordinates((source, self.triangle_edge_length - 1 - i, 0), (destination, i, 0))

    def strip_right_to_uleft(self, source, destination):
        for i in range(0, self.triangle_edge_length):
            self.update_coordinates((source, i, 0), (destination, self.triangle_edge_length - 1 - i, 0))

    def validate_corner(self, t, q, r):
        corner_node = self.get_node(t, q, r)
        if corner_node is None:
            raise ValueError("The node ({}, {}, {},) doesn't exist".format(t, q, r))
        else:
            corner_node.assert_corner()

        return corner_node

    def update_coordinates(self, source_coords, new_coords):
        node = self.get_node(*source_coords)
        node.add_coordinates(*new_coords)
        self.nodes[new_coords] = node



