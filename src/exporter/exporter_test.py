import unittest
import json
from src.mapgenerators.heightmap import HeightMap
from src.nodemapper.node_mapper import NodeMapper
from src.exporter.exporter import Exporter

class TestExporter(unittest.TestCase):
    def test_export_large_world_map_for_tiled(self):
        known_output = [0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 0, 0, 0, 5, 0, 0, 0, 10, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 5, 0, 0, 10, 10, 0, 0, 10, 10, 0, 0, 10, 10, 0, 0, 10, 0, 0, 0, 10, 10, 10, 5, 0, 10, 10, 10, 0, 10, 10, 10, 0, 10, 10, 10, 0, 10, 10, 0, 0, 0, 5, 10, 10, 5, 10, 10, 10, 10, 5, 10, 10, 10, 5, 10, 10, 10, 10, 10, 10, 10, 0, 0, 5, 10, 10, 5, 10, 10, 10, 5, 5, 10, 10, 10, 10, 10, 10, 10, 5, 5, 10, 10, 0, 0, 0, 5, 5, 5, 5, 5, 10, 5, 10, 5, 10, 10, 10, 10, 10, 10, 5, 5, 10, 10, 10, 0, 0, 10, 10, 5, 5, 5, 5, 5, 5, 5, 10, 10, 10, 10, 10, 10, 5, 5, 10, 10, 5, 0, 0, 0, 10, 5, 5, 5, 10, 10, 10, 5, 10, 10, 10, 10, 10, 5, 5, 5, 10, 10, 10, 5, 0, 0, 10, 5, 5, 10, 0, 10, 10, 10, 0, 10, 10, 10, 0, 5, 5, 10, 0, 10, 10, 0, 0, 0, 0, 10, 10, 10, 0, 0, 10, 10, 0, 0, 10, 5, 0, 0, 10, 10, 0, 0, 10, 0, 0, 0, 0, 10, 10, 0, 0, 0, 10, 0, 0, 0, 5, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        test_seed = 1000000001
        mapper = NodeMapper(triangle_edge=5)
        height_mapper = HeightMap(mapper, size=5, seed_value=test_seed)
        height_mapper.make_world(20)
        height_mapper.set_sea_level(.30)
        height_mapper.get_stats()
        exporter = Exporter(mapper)
        exporter.export_large_world_map_for_tiled(map_seed=1000000001)
        map_template = json.loads(open("seed_1000000001/large_map.json").read())
        self.assertEqual(known_output, map_template["layers"][0]["data"])