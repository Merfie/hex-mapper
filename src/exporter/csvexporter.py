import csv
from src.nodemapper.node_mapper import NodeMapper

class CSVExporter(object):

    def export_world(self, world):
        with open('nodes-513.csv', 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(["P", "Q", "R"])
            for node in world.nodes.keys():
                writer.writerow([node[0], node[1], node[2]])

def generate_and_export():
    exporter = CSVExporter()
    world = NodeMapper(512)
    make_world(world)
    exporter.export_world(world)

def make_world(node_map):
    for i in range(0, 20):
        node_map.make_triangle(i)
        if i is 0:
            pass
        elif i < 5:
            node_map.strip_right_to_left(i - 1, i)
            if i is 4:
                node_map.strip_left_to_right(0, i)
        elif i is 5:
            node_map.strip_bottom_to_bottom(0, i)
        elif i < 15:
            if i % 2 is 0:
                node_map.strip_uright_to_left(i - 1, i)
                if i is 14:
                    node_map.strip_uleft_to_right(5, 14)
            else:
                node_map.strip_bottom_to_bottom((i - 5)/2, i)
                node_map.strip_right_to_uleft(i - 1, i)
        else:
            node_map.strip_bottom_to_bottom((i - 12) * 2, i)
            if(i > 15):
                node_map.strip_left_to_right(i - 1, i)
            if i is 19:
                node_map.strip_right_to_left(15, i)
