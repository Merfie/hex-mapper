import json
import os
from PIL import Image, ImageDraw
import math
class Exporter(object):

    """
        An object to export a mapper object into various forms
    """

    def __init__(self, mapper, max_height, min_height, sea_level, min_temp, max_temp):
        self.mapper = mapper
        self.max_height = max_height
        self.min_height = min_height
        self.sea_level = sea_level
        self.heightmap_colors = []
        self.max_temp = max_temp
        self.min_temp = min_temp
        self.heatmap_colors = [(0,254,255), (164,254,255),(0,227,132), (250,255,96), (255,98,0), (239,17,0)]

    def export_large_world_map_for_tiled(self, map_seed):
        edge_size = self.mapper.triangle_edge_length - 1
        map_template = json.loads(open(str(self.mapper.triangle_edge_length) + "_template_2d_map.json").read())
        map_width = int((self.mapper.triangle_edge_length - 1) * 5.5)
        for node in set(self.mapper.nodes.values()):
            x_offset = 0
            y_offset = 0
            t = node.coordinates[0][0]
            q = node.coordinates[0][1]
            r = node.coordinates[0][2]
            if t < 5:
                x_offset = edge_size * t
                y_offset = 0
                x = x_offset + q + int((edge_size - (q + r))/2)
                y = y_offset + q + r
            elif t < 15:
                y_offset = edge_size
                if t % 2 is 0:
                    y = y_offset + q + r
                    x_offset = int(edge_size/2) * (t - 5)
                    x = x_offset + q + int((edge_size - (q + r))/2)
                else:
                    y = y_offset + (edge_size - (q + r))
                    x_offset = int(edge_size/2) * (t - 5)
                    x = x_offset + int((edge_size - (q + r))/2) + ((q + r) - q)
            else:
                y_offset = edge_size * 2
                x_offset = int(edge_size/2) + (edge_size * (t - 15))
                y = y_offset + (edge_size - (q + r))
                x = x_offset + int((edge_size - (q + r))/2) + ((q + r) - q)

            map_template["layers"][0]["data"][(y * map_width) + x] = node.get_attribute("biome")
            map_template["layers"][1]["data"][(y * map_width) + x] = node.get_attribute("terrain")

        directory = "map_" + str(map_seed)
        if not os.path.exists(directory):
            os.makedirs(directory)
        with open(str(directory) + '/large_map.json', 'w') as outfile:
            json.dump(map_template, outfile)

    def export_small_world_map_image(self, map_seed):
        edge_size = self.mapper.triangle_edge_length - 1
        img = Image.new("RGBA", (int(edge_size * 5.5), int(edge_size * 3)), (255, 255, 255, 0))
        draw = ImageDraw.Draw(img)
        x = 0
        y = 0
        for node in set(self.mapper.nodes.values()):
            x_offset = 0
            y_offset = 0
            t = node.coordinates[0][0]
            q = node.coordinates[0][1]
            r = node.coordinates[0][2]
            if t < 5:
                x_offset = edge_size * t
                y_offset = 0
                x = x_offset + q + int((edge_size - (q + r)) / 2)
                y = y_offset + q + r
            elif t < 15:
                y_offset = edge_size
                if t % 2 is 0:
                    y = y_offset + q + r
                    x_offset = int(edge_size / 2) * (t - 5)
                    x = x_offset + q + int((512 - (q + r)) / 2)
                else:
                    y = y_offset + (edge_size - (q + r))
                    x_offset = int(edge_size / 2) * (t - 5)
                    x = x_offset + int((edge_size - (q + r)) / 2) + ((q + r) - q)
            else:
                y_offset = edge_size * 2
                x_offset = int(edge_size / 2) + (edge_size * (t - 15))
                y = y_offset + (edge_size - (q + r))
                x = x_offset + int((edge_size - (q + r)) / 2) + ((q + r) - q)

            draw.point((x, y), self.get_biome_color(node.get_attribute("biome")))
        directory = "map_" + str(map_seed)
        img.save(str(directory) + '/' + str(map_seed) + '.png')

    def export_small_world_map_projection(self, map_seed):
        edge_size = self.mapper.triangle_edge_length - 1
        img = Image.new("RGBA", (int(edge_size * 6 * 16), int(edge_size * 3 * 16)), (255, 255, 255, 0))
        draw = ImageDraw.Draw(img)
        x = 0
        y = 0
        for node in set(self.mapper.nodes.values()):
            x_offset = 0
            y_offset = 0
            t = node.coordinates[0][0]
            q = node.coordinates[0][1]
            r = node.coordinates[0][2]
            if t < 5:
                x_offset = edge_size * t * 16
                y_offset = 0
                x = x_offset + q + int((edge_size - (q + r)) / 2)
                y = y_offset + q + r
            elif t < 15:
                y_offset = edge_size * 16
                if t % 2 is 0:
                    y = y_offset + q + r
                    x_offset = (int(edge_size / 2)) * 16
                    x = x_offset + q + int((512 - (q + r)) / 2)
                else:
                    y = y_offset + (edge_size - (q + r))
                    x_offset = int(edge_size / 2)
                    x = x_offset + int((edge_size - (q + r)) / 2) + ((q + r) - q)
            else:
                y_offset = edge_size * 2 * 16
                x_offset = (int(edge_size / 2) + (edge_size * (t - 15))) * 16
                y = y_offset + (edge_size - (q + r))
                x = x_offset + int((edge_size - (q + r)) / 2) + ((q + r) - q)
            x = x * 16
            y = y * 16
            draw.rectangle([(x, y), (x + 16, y + 16)], self.get_biome_color(node.get_attribute("biome")))
        directory = "map_" + str(map_seed)
        img.save(str(directory) + '/' + str(map_seed) + '_projected.png')

    def generate_gradient(self, start_color, end_color, steps):
        red_step = (end_color[0] - start_color[0]) / steps
        green_step = (end_color[1] - start_color[1]) / steps
        blue_step = (end_color[2] - start_color[2]) / steps

        colors = []
        for i in range(0, steps):
            colors.append((int(start_color[0] + red_step * i), int(start_color[1] + green_step * i),
                           int(start_color[2] + blue_step * i)))
        return colors

    def gen_heightmap_colors(self):
        base_colors = [(0, 132, 53), (51, 204, 0), (244, 189, 69), (153, 100, 4), (255, 255, 255)]
        steps = int(20 / (len(base_colors) - 1))
        for i in range(0, len(base_colors) - 1):
            self.heightmap_colors += self.generate_gradient(base_colors[i], base_colors[i + 1], steps)

    def get_height_map_color(self, height):
        if height <= 0:
            return (0, 128, 255)
        else:
            percent = int((height) / (self.max_height) * len(self.heightmap_colors) - 1)
            return self.heightmap_colors[percent]

    def gen_tempmap_colors(self):
        steps = int(5 / (len(self.heatmap_colors) - 1))
        for i in range(0, len(self.heatmap_colors) - 1):
            self.heightmap_colors += self.generate_gradient(self.heatmap_colors[i], self.heatmap_colors[i + 1], steps)

    def get_tempmap_map_color(self, temp):
        percent = (temp - self.min_temp) / (self.max_temp - self.min_temp)
        if percent < .166:
            return self.heatmap_colors[0]
        elif percent < .332:
            return self.heatmap_colors[1]
        elif percent < .498:
            return self.heatmap_colors[2]
        elif percent < .664:
            return self.heatmap_colors[3]
        elif percent < .83:
            return self.heatmap_colors[4]
        else:
            return self.heatmap_colors[5]

    def get_biome_color(self, biome):
        if biome is 1:
            return (255, 255, 153)
        elif biome is 2:
            return (0, 51, 102)
        elif biome is 3:
            return (255, 204, 102)
        elif biome is 4:
            return (102, 153, 0)
        elif biome is 5:
            return (153, 255, 102)
        elif biome is 6:
            return (255, 153, 51)
        elif biome is 7:
            return (51, 102, 0)
        elif biome is 8:
            return (0, 102, 153)
        elif biome is 9:
            return (51, 204, 204)
        elif biome is 10:
            return (255, 255, 255)
        elif biome is 11:
            return (204, 255, 204)
        elif biome is 12:
            return (244, 255, 255)

    def export_large_individual_triangles_projected(self, number_of_triangles, map_seed):
        for i in range(0, number_of_triangles):
            triangle = self.mapper.get_triangle(i)
            img = Image.new("RGBA", (513 * 16, 513 * 16), (255, 255, 255, 0))
            heightmap = Image.new("RGBA", (513 * 16, 513 * 16), (255, 255, 255, 0))
            draw = ImageDraw.Draw(img)
            draw2 = ImageDraw.Draw(heightmap)
            x = 0
            y = 0
            offset = 0
            edge_size = self.mapper.triangle_edge_length
            for node in triangle:
                q = node.get_coordinates_for_triangle(i)[1]
                r = node.get_coordinates_for_triangle(i)[2]
                y = q + r
                x = q

                if i < 5:
                    offset = edge_size / (y + 1)
                    draw.rectangle([((x * offset) * 16, y * 16), ((x * offset + offset) * 16, (y + 1) * 16)],
                                   self.get_biome_color(node.get_attribute("biome")))
                    draw2.rectangle([((x * offset) * 16, y * 16), ((x * offset + offset) * 16, (y + 1) * 16)],
                                    self.get_height_map(node.get_attribute("biome"), node.get_attribute("height")))
                elif i < 15:
                    y = (edge_size - (q + r))
                    x = int((edge_size - (q + r)) / 2) + ((q + r) - q)
                    draw.rectangle([(x * 16, y * 16), (x * 16 + 16, y * 16 + 16)],
                                   self.get_biome_color(node.get_attribute("biome")))
                    draw2.rectangle([(x * 16, y * 16), (x * 16 + 16, y * 16 + 16)],
                                    self.get_height_map(node.get_attribute("biome"), node.get_attribute("height")))
                else:
                    offset = edge_size / (y + 1)
                    draw.rectangle([((x * offset) * 16, y * 16), ((x * offset + offset) * 16, (y + 1) * 16)],
                                   self.get_biome_color(node.get_attribute("biome")))
                    draw2.rectangle([((x * offset) * 16, y * 16), ((x * offset + offset) * 16, (y + 1) * 16)],
                                    self.get_height_map(node.get_attribute("biome"), node.get_attribute("height")))
            directory = "map_" + str(map_seed)
            directory = "map_" + str(map_seed)
            if not os.path.exists(directory):
                os.makedirs(directory)
            img.save(str(directory) + '/' + str(map_seed) + '_projected_' + str(i) + '.png')
            heightmap.save(str(directory) + '/' + str(map_seed) + '_projected_heightmap_' + str(i) + '.png')

    def export_small_projected_heightmap(self, number_of_triangles, edge_size, map_seed):
        self.gen_heightmap_colors()
        compressed_size = 128
        img = Image.new("RGBA", (compressed_size * 6, compressed_size * 3), (255, 255, 255, 0))
        directory = "map_" + str(map_seed)
        if not os.path.exists(directory):
            os.makedirs(directory)
        draw = ImageDraw.Draw(img)
        t_counter = -1
        map_y_counter = -1
        for t in range(0, number_of_triangles):
            x_offset = 0
            x_offset_2 = 0
            y_offset = 0
            y_offset_2 = 0
            triangle_y = 0
            map_y = 0
            map_y_counter = -1
            while triangle_y <= edge_size:
                triangle_x = 0
                map_x = 0
                while triangle_x <= triangle_y:
                    node = self.mapper.get_node_by_xy(t, triangle_x, triangle_y)
                    x = 0
                    y = 0
                    if t is not t_counter:
                        t_counter = t
                        if t < 5:
                            x_offset = compressed_size * t
                            y_offset = 0
                        elif t < 15:
                            y_offset = compressed_size
                            x_offset = (int(compressed_size / 2)) * (t - 5)
                        else:
                            y_offset = compressed_size * 2
                            x_offset = int(compressed_size / 2) + (compressed_size * (t - 15))
                    if map_y is not map_y_counter:
                        map_y_counter = map_y
                        if t < 5:
                            x_offset_2 = int((compressed_size - map_y) / 2)
                        elif t < 15:
                            if t % 2 is 0:
                                x_offset_2 = int((compressed_size - map_y) / 2)
                            else:
                                y_offset_2 = (compressed_size - map_y)
                                x_offset_2 = int((compressed_size - map_y) / 2)
                        else:
                            y_offset_2 = (compressed_size - map_y)
                            x_offset_2 = int((compressed_size - map_y) / 2)
                    if t < 5:
                        x = x_offset + x_offset_2 + map_x
                        y = map_y
                    elif t < 15:
                        if t % 2 is 0:
                            y = y_offset + map_y
                            x = x_offset + x_offset_2 + map_x
                        else:
                            y = y_offset + y_offset_2
                            x = x_offset + x_offset_2 + (map_y - map_x)
                    else:
                        y = y_offset + y_offset_2
                        x = x_offset + x_offset_2 + (map_y - map_x)
                    draw.point((x, y), self.get_height_map_color(node.get_attribute("height")))
                    triangle_x += int(edge_size / compressed_size)
                    map_x += 1
                triangle_y += int(edge_size / compressed_size)
                map_y += 1
        img.save(str(directory) + '/' + str(map_seed) + '_projected_heightmap.jpg')

    def export_small_projected_tempmap(self, number_of_triangles, edge_size, map_seed):
        self.gen_tempmap_colors()
        print(str(self.min_temp) + " " + str(self.max_temp))
        compressed_size = 128
        img = Image.new("RGBA", (compressed_size * 6, compressed_size * 3), (255, 255, 255, 0))
        directory = "map_" + str(map_seed)
        if not os.path.exists(directory):
            os.makedirs(directory)
        draw = ImageDraw.Draw(img)
        t_counter = -1
        map_y_counter = -1
        for t in range(0, number_of_triangles):
            x_offset = 0
            x_offset_2 = 0
            y_offset = 0
            y_offset_2 = 0
            triangle_y = 0
            map_y = 0
            map_y_counter = -1
            while triangle_y <= edge_size:
                triangle_x = 0
                map_x = 0
                while triangle_x <= triangle_y:
                    node = self.mapper.get_node_by_xy(t, triangle_x, triangle_y)
                    x = 0
                    y = 0
                    if t is not t_counter:
                        t_counter = t
                        if t < 5:
                            x_offset = compressed_size * t
                            y_offset = 0
                        elif t < 15:
                            y_offset = compressed_size
                            x_offset = (int(compressed_size / 2)) * (t - 5)
                        else:
                            y_offset = compressed_size * 2
                            x_offset = int(compressed_size / 2) + (compressed_size * (t - 15))
                    if map_y is not map_y_counter:
                        map_y_counter = map_y
                        if t < 5:
                            x_offset_2 = int((compressed_size - map_y) / 2)
                        elif t < 15:
                            if t % 2 is 0:
                                x_offset_2 = int((compressed_size - map_y) / 2)
                            else:
                                y_offset_2 = (compressed_size - map_y)
                                x_offset_2 = int((compressed_size - map_y) / 2)
                        else:
                            y_offset_2 = (compressed_size - map_y)
                            x_offset_2 = int((compressed_size - map_y) / 2)
                    if t < 5:
                        x = x_offset + x_offset_2 + map_x
                        y = map_y
                    elif t < 15:
                        if t % 2 is 0:
                            y = y_offset + map_y
                            x = x_offset + x_offset_2 + map_x
                        else:
                            y = y_offset + y_offset_2
                            x = x_offset + x_offset_2 + (map_y - map_x)
                    else:
                        y = y_offset + y_offset_2
                        x = x_offset + x_offset_2 + (map_y - map_x)
                    draw.point((x, y), self.get_tempmap_map_color(node.get_attribute("temperature")))
                    triangle_x += int(edge_size / compressed_size)
                    map_x += 1
                triangle_y += int(edge_size / compressed_size)
                map_y += 1
        img.save(str(directory) + '/' + str(map_seed) + '_projected_tempmap.jpg')

